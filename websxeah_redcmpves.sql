-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 13-10-2021 a las 23:12:21
-- Versión del servidor: 10.3.31-MariaDB-log-cll-lve
-- Versión de PHP: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `websxeah_redcmpves`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `nombre`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Noticias', NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(2, 'Resultados', NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(3, 'Eventos', NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(4, 'Proyectos', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos`
--

CREATE TABLE `documentos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `documentos`
--

INSERT INTO `documentos` (`id`, `nombre`, `slug`, `url`, `tipo_id`, `descripcion`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Apuntes y Mensajes Para Familias Miles de Manos', 'apuntes-y-mensajes-para-familias-miles-de-manos', 'apuntes_y_mensajes_para_familias_miles_de_manos.pdf', '3', NULL, NULL, '2018-10-10 23:11:48', '2018-10-15 20:12:40'),
(2, 'Bases y Orientaciones', 'bases-y-orientaciones', 'bases_y_orientaciones.pdf', '3', NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(3, 'Código Municipal', 'codigo-municipal', 'codigo_municipal.pdf', '1', NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(4, 'Estrategia Nacional de Prevención de Violencia', 'estrategia-nacional-de-prevencion-de-violencia', 'estategia_nacional_de_prevencion_de_violencia.pdf', '1', NULL, NULL, '2018-10-10 23:11:48', '2018-10-16 00:37:41'),
(5, 'Guía de Facilitación Componente Escuela Miles de Manos', 'guia-de-facilitacion-componente-escuela-miles-de-manos', 'guia_de_facilitacion_componente_escuela_miles_de_manos.pdf', '3', NULL, NULL, '2018-10-10 23:11:48', '2018-10-15 20:11:49'),
(6, 'Quía de Facilitación Componente Familia', 'quia-de-facilitacion-componente-familia', 'guia_de_facilitacion_componente_familia.pdf', '3', NULL, NULL, '2018-10-10 23:11:48', '2018-10-15 20:12:17'),
(7, 'Guía de Facilitación Componente Puente Miles de Manos', 'guia-de-facilitacion-componente-puente-miles-de-manos', 'guia_de_facilitacion_componente_puente_miles_de_manos.pdf', '3', NULL, NULL, '2018-10-10 23:11:48', '2018-10-15 20:10:54'),
(8, 'Ley Especial Integral Para una Vida Libre de Violencia Hacia las Mujeres', 'ley-especial-integral-para-una-vida-libre-de-violencia-hacia-las-mujeres', 'ley_especial_integral_para_una_vida_libre_de_violencia_hacia_las_mujeres.pdf', '1', NULL, NULL, '2018-10-10 23:11:48', '2018-10-15 20:10:23'),
(9, 'Ley FODES', 'ley-fodes', 'ley_fodes.pdf', '1', NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(10, 'Ley General de Juventud y su Reglamento', 'ley-general-de-juventud-y-su-reglamento', 'ley_general_de_juventud_y_su_reglamento.pdf', '1', NULL, NULL, '2018-10-10 23:11:48', '2018-10-15 20:09:56'),
(11, 'Ley LEPINA', 'ley-lepina', 'ley_lepina.pdf', '1', NULL, NULL, '2018-10-10 23:11:48', '2018-10-16 00:37:17'),
(12, 'Ley Marco Para la Convivencia Ciudadana y Contravenciones Administrativas', 'ley-marco-para-la-convivencia-ciudadana-y-contravenciones-administrativas', 'ley_marco_para_la_convivencia_ciudadana_y_contraversiones_administrativas.pdf', '1', NULL, NULL, '2018-10-10 23:11:48', '2018-10-16 00:37:02'),
(13, 'Material de Apoyo Guías de Práctica para Docentes', 'material-de-apoyo-guias-de-practica-para-docentes', 'material_de_apoyo_guias_de_practica_para_docentes.pdf', '3', NULL, NULL, '2018-10-10 23:11:48', '2018-10-15 19:43:03'),
(14, 'Manual Operativo de los Servicios Publicos de Empleo', 'manual-operativo-de-los-servicios-publicos-de-empleo', 'manual_operativo_de_los_servicios_publicos_de_empleo.pdf', '4', NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(15, 'Plan El Salvador Seguro', 'plan-el-salvador-seguro', 'pla_el_salvador_seguro_pess.pdf', '2', NULL, '2018-10-24 21:19:34', '2018-10-10 23:11:48', '2018-10-24 21:19:34'),
(16, 'Política de Cohesión Social y Prevención AMSS', 'politica-de-cohesion-social-y-prevencion-amss', 'politica_de_cohesion_social_y_prevencion_amss.pdf', '1', NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(17, 'Política Nacional de Justicia', 'politica-nacional-de-justicia', 'politica_nacional_de_justicia.pdf', '1', NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(18, 'Política Nacional de Juventud', 'politica-nacional-de-juventud', 'politica_nacional_de_juventud.pdf', '1', NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(19, 'Anteproyecto de Ley del Sistema Nacional de Prevención de la Violencia', 'anteproyecto-de-ley-del-sistema-nacional-de-prevencion-de-la-violencia', 'ley_del_sistema_nacional_de_prevencion_de_la_violencia.pdf', '1', NULL, NULL, '2018-10-11 21:40:25', '2018-10-16 00:36:28'),
(20, 'Constitución de la República de El Salvador', 'constitucion-de-la-republica-de-el-salvador', 'constitucion_de_la_republica_de_el_salvador.pdf', '1', NULL, NULL, '2018-10-19 02:54:43', '2018-10-19 02:54:43'),
(21, 'PLAN EL SALVADOR SEGURO (PESS)', 'plan-el-salvador-seguro-pess', 'plan_el_salvador_seguro_pess.pdf', '2', 'Documento completo de Plan El Salvador Seguro', NULL, '2018-10-24 21:29:01', '2018-10-24 21:29:01'),
(22, 'APORTES AL ANTEPROYECTO DE LEY DEL SISTEMA NACIONAL DE PREVENCIÓN DE LA VIOLENCIA DE EL SALVADOR', 'aportes-al-anteproyecto-de-ley-del-sistema-nacional-de-prevencion-de-la-violencia-de-el-salvador', 'aportes_al_anteproyecto_de_ley_del_sistema_nacional_de_prevencion_de_la_violencia_de_el_salvador.pdf', '1', 'Este documento contiene los aportes que la Red de Comités Municipales de Prevención de la Violencia para avanzar hacia la aprobación del SINAPREV', NULL, '2018-11-09 18:28:15', '2018-11-09 18:28:15'),
(23, 'MODELO DE REGLAMENTO INTERNO PARA COMITÉS MUNICIPALES DE PREVENCIÓN DE LA VIOLENCIA', 'modelo-de-reglamento-interno-para-comites-municipales-de-prevencion-de-la-violencia', 'modelo_de_reglamento_interno_para_comites_municipales_de_prevencion_de_la_violencia.pdf', '1', 'A continuación se presenta un modelo de reglamento interno para Comités Municipales de Prevención de la Violencia, cada comité podrá adecuarlo a sus necesidades y desafíos.', NULL, '2018-11-21 03:12:07', '2018-11-21 03:12:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'logo.png',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`id`, `nombre`, `logo`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'RedCDMYPE', 'logo.png', NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enlaces`
--

CREATE TABLE `enlaces` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `enlaces`
--

INSERT INTO `enlaces` (`id`, `nombre`, `url`, `tipo_id`, `descripcion`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Caja de Herramientas PREVENIR', 'http://www.gizprevenir.com/cajadeherramientas', '2', 'Caja de Herramientas para la cooperación intersectorial en prevención de la violencia a nivel local.', NULL, '2018-10-10 23:11:48', '2018-10-16 01:10:39'),
(2, 'Tu Chance', 'https://tuchance.org/sv', '2', 'Es una ventana virtual a la oferta de oportunidades de formación, becas, empleos, entrenamiento y capacitación.', NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(3, 'Ministerio de Justicia y Seguridad Pública', 'http://www.seguridad.gob.sv/', '3', NULL, NULL, '2018-10-19 20:03:03', '2018-10-19 20:03:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipos`
--

CREATE TABLE `equipos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cargo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `equipos`
--

INSERT INTO `equipos` (`id`, `nombre`, `img`, `cargo`, `twitter`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Nelson Villalta', 'nelson.jpg', 'Coordinador General Nacional', 'Nelson', NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(2, 'Roxy Acosta', 'roxy.jpg', 'Sub Coordinadora Nacional', 'Roxy', NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(3, 'Patty', 'patty.jpg', 'Secretaria', 'Patty', NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(4, 'Guillermo Zepeda', 'guillermo.jpg', 'Tesorero', 'Guillermo', NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(5, 'Wigberto Castillo', 'wigberto.jpg', 'Coordinador Secretaría Comunicación', 'Wigberto', NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(6, 'Juan Jose Hernandez', 'jose.jpg', 'Coordinador de Equipo de Gestión', 'Juan', NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `miembros`
--

CREATE TABLE `miembros` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `correo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `departamento` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url_facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url_web` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `miembros`
--

INSERT INTO `miembros` (`id`, `nombre`, `slug`, `img`, `telefono`, `correo`, `departamento`, `direccion`, `url_facebook`, `url_web`, `coords`, `lat`, `lon`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Mejicanos', 'mejicanos', 'mejicanos.jpeg', '', '', 'San Salvador', NULL, 'cmpv.mejicanos', NULL, '462,317,484,343', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(2, 'Ilobasco', 'ilobasco', 'ilobasco.jpeg', '2362-6700', '', 'Cabañas', NULL, 'AlcaldiaIlobasco', NULL, '696,230,723,263', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(3, 'Apopa', 'apopa', 'apopa.jpeg', '', '', 'San Salvador', NULL, 'CMPVApopa', NULL, '435,290,463,315', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(4, 'Chalchuapa', 'chalchuapa', 'chalchuapa.jpeg', '2461-0568', '', 'Santa Ana', NULL, 'AlcaldiaChalchuapaOficial', NULL, '217,181,250,209', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(5, 'Armenia', 'armenia', 'armenia.jpeg', '', '', 'Sonsonate', NULL, 'Alcaldía-Municipal-de-Armenia-Sonsonate-El-Salvador-470689403005884/', NULL, '291,310,319,337', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(6, 'Acajutla', 'acajutla', 'acajutla.jpeg', '2452-3541-2429 7300', NULL, 'Sonsonate', 'Blvd. 25 de Febrero, Acajutla.', 'Alcaldia de Acajutla', NULL, '159,409,187,436', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-11-02 04:18:30'),
(7, 'Usulután', 'usulutan', 'usulutan.jpg', '2624-4864', 'cmpvjusulutan@gmail.com', 'Usulután', NULL, 'AlcaldiaUsulutan', NULL, '781,475,811,507', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-10-19 03:27:00'),
(8, 'Guazapa', 'guazapa', 'guazapa.jpg', '2324-0001', '', 'San Salvador', NULL, 'alcaldiaguazapa', NULL, '465,239,497,265', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(9, 'Ciudad Delgado', 'ciudad-delgado', 'delgado.jpeg', '2561-2100', '', 'San Salvador', NULL, 'AlcaldiaCiudadDelgadoSV', NULL, '491,295,510,331', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(10, 'Olocuilta', 'olocuilta', 'olocuilta.jpg', '2561-2100', '', 'La Paz', NULL, 'AlcaldiaDeOlocuilta', NULL, '495,412,523,440', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(11, 'Santo Tomás', 'santo-tomas', 'santo_tomas.jpg', '2213-3100', 'alcaldiadesantotomas@gmail.com', 'San Salvador', NULL, 'AlcaldiaMunicipalDeSantoTomas', NULL, '479,365,511,395', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-10-19 03:23:09'),
(12, 'Cojutepeque', 'cojutepeque', 'cojutepeque.jpg', '2379-3900', 'info@cojutepeque.gob.sv', 'Cuscatlan', NULL, 'AlcaldiaMunicipalCojutepeque', NULL, '590,319,621,353', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-11-02 04:03:35'),
(13, 'San Vicente', 'san-vicente', 'san_vicente.jpeg', '2314-2400', 'cmpv@amsv.gob.sv', 'San Vicente', '1a. Calle Pte. y 1a. Ave. Nte. 1701', 'CMPVSanVicente', NULL, '704,354,733,387', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-11-02 03:26:29'),
(14, 'Tecoluca', 'tecoluca', 'tecoluca.jpg', '', '', 'San Vicente', NULL, 'CMPV-Tecoluca-804889826294075', NULL, '655,455,685,487', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(15, 'Ahuachapán', 'ahuachapan', 'ahuachapan.jpg', '2487-4800', NULL, 'Ahuachapán', NULL, 'alcaldiadeahuachapan', NULL, '123,251,146,270', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-10-19 03:28:32'),
(16, 'Sonsonate', 'sonsonate', 'sonsonate.jpg', '2469 3100', 'comunicacionessonsonate@gmail.com', 'Sonsonate', 'Av. Claudia Lars 1-1 Sonsonate', 'Alcaldía-Municipal-de-Sonsonate-191014414284574', NULL, '223,387,255,415', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-11-02 04:13:20'),
(17, 'Zacatecoluca', 'zacatecoluca', 'zacatecoluca.jpg', '2334-7900', 'alcaldiadezacatecoluca@gmail.com', 'La Paz', NULL, 'alcaldiamunicipaldezacatecoluca', NULL, '613,510,643,538', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(18, 'Quezaltepeque', 'quezaltepeque', 'quezaltepeque.jpg', '2343-6700', '', 'La Libertad', NULL, 'AlcaldiaQZ', NULL, '395,286,427,314', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(19, 'La Unión', 'la-union', 'la_union.jpg', '2609-7000', '', 'La Unión', NULL, 'AcaldiaLaUnion', NULL, '1111,553,1137,587', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(20, 'Izalco', 'izalco', 'izalco.jpg', '2429-8000', '', 'Sonsonate', NULL, 'IzalcoAlcaldia', NULL, '210,312,237,343', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(21, 'Conchagua', 'conchagua', 'conchagua.jpg', '2429-8000', '', 'La Unión', NULL, 'IzalcoAlcaldia', NULL, '1081,575,1109,610', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(22, 'Santa Cruz Michapa', 'santa-cruz-michapa', 'michapa.jpg', NULL, NULL, 'Cuscatlan', NULL, 'AlcaldiaMichapaOficial', NULL, '548,319,577,351', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-11-02 03:54:10'),
(23, 'San Salvador', 'san-salvador', 'san_salvador.jpg', '2511-6000', '', 'San Salvador', NULL, 'AlcaldiaDeSanSalvador', NULL, '428,333,459,364', NULL, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `miembros_documentos`
--

CREATE TABLE `miembros_documentos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `miembro_id` int(11) NOT NULL,
  `descripcion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `miembros_documentos`
--

INSERT INTO `miembros_documentos` (`id`, `nombre`, `slug`, `url`, `tipo_id`, `miembro_id`, `descripcion`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Diagnóstico y Plan Ahuachapán Seguro', 'diagnostico-y-plan-ahuachapan-seguro', 'ahuachapan_diagnostico_y_plan_ahuachapan_seguro.pdf', '2', 15, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(2, 'Diagnóstico Municipal de la Violencia de Cojutepeque', 'diagnostico-municipal-de-la-violencia-de-cojutepeque', 'cojutepeque_diagnostico_municipal_de_la_violencia_de_cojutepeque.pdf', '2', 12, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(3, 'Plan Cojute Seguro', 'plan-cojute-seguro', 'cojutepeque_plan_cojute_seguro.pdf', '2', 12, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(4, 'Plan Municipal de Prevención de Violencia Cojutepeque', 'plan-municipal-de-prevencion-de-violencia-cojutepeque', 'cojutepeque_plan_municipal_de_prevencion_de_violencia_cojutepeque.pdf', '2', 12, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(5, 'Política Municipal de Prevención de Violencia', 'politica-municipal-de-prevencion-de-violencia', 'cojutepeque_politica_municipal_de_prevencion_de_violencia.pdf', '1', 12, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(6, 'Diagnóstico de Violencia de Conchagua', 'diagnostico-de-violencia-de-conchagua', 'conchagua_diagnostico_de_violencia_de_conchagua.pdf', '2', 21, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(7, 'PMPV Cojutepeque Seguro', 'pmpv-cojutepeque-seguro', 'cojutepeque_pmpv_cojutepeque_seguro.pdf', '1', 12, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(8, 'Diagnóstico de Guazapa', 'diagnostico-de-guazapa', 'guazapa_diagnostico_de_guazapa.pdf', '2', 8, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(9, 'Plan de Trabajo de Mural', 'plan-de-trabajo-de-mural', 'guazapa_plan_de_trabajo_de_mural.pdf', '2', 8, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(10, 'Plan de Trabajo Orquesta Filarmónica', 'plan-de-trabajo-orquesta-filarmonica', 'guazapa_plan_de_trabajo_orquesta_filarmonica.pdf', '2', 8, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(11, 'Plan Guazapa Seguro', 'plan-guazapa-seguro', 'guazapa_plan_guazapa_seguro.pdf', '2', 8, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(12, 'PMPV de Guazapa', 'pmpv-de-guazapa', 'guazapa_pmpv_de_guazapa.pdf', '2', 8, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(13, 'Plan Ilobasco Seguro', 'plan-ilobasco-seguro', 'ilobasco_plan_ilobasco_seguro.pdf', '2', 2, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(14, 'Ordenanza Contravensional para la Convivencia y Seguridad', 'ordenanza-contravensional-para-la-convivencia-y-seguridad', 'santa-cruz-michapa_ordenanza_contravensional_para_la_convivencia_y_seguridad.pdf', '2', 22, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(15, 'Plan El Salvador Seguro Santa Cruz Michapa', 'plan-el-salvador-seguro-santa-cruz-michapa', 'santa-cruz-michapa_plan_el_salvador_seguro_santa_cruz_michapa.pdf', '2', 22, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(16, 'PMPV San Vivente Seguro', 'pmpv-san-vivente-seguro', 'san-vicente_pmpv_san_vivente_seguro.pdf', '1', 13, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(17, 'Política Municipal de Prevención de la Violencia San Vincente', 'politica-municipal-de-prevencion-de-la-violencia-san-vincente', 'san-vicente_politica_municipal_de_prevencion_de_la_violencia_san_vincente.pdf', '1', 13, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(18, 'Diagnostico Situacional de las Mujeres', 'diagnostico_situacional_de_las_mujeres', 'armenia_diagnostico_situacional_de_las_mujeres.pdf', '2', 5, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(19, 'Estatutos de MEPERSA', 'estatutos_de_mepersa', 'estatutos_de_mepersa.pdf', '1', 5, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(20, 'Experiencia Exitosa de la Liga Atletica Municipal Armenia', 'experiencia_exitosa_de_la_liga_atletica_municipal_armenia', 'armenia_experiencia_exitosa_de_la_liga_atletica_municipal_armenia.pdf', '1', 5, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(21, 'Ordenanza Reguladora de Uso de los Sitios Públicos', 'ordenanza_reguladora_de_uso_de_los_sitios_publicos', 'armenia_ordenanza_reguladora_de_uso_de_los_sitios_publicos.pdf', '1', 5, NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(29, 'Diagnostico Municipal de la Violencia de Usulután', 'diagnostico-municipal-de-la-violencia-de-usulutan', 'diagnostico_municipal_de_la_violencia_de_usulutan.pdf', '2', 7, NULL, NULL, '2018-10-11 00:13:35', '2018-10-11 00:13:35'),
(30, 'Plan El Salvador Seguro Usulután', 'plan-el-salvador-seguro-usulutan', 'plan_el_salvador_seguro_usulutan.pdf', '2', 7, NULL, NULL, '2018-10-11 00:38:21', '2018-10-11 00:38:21'),
(31, 'Plan Zacatecoluca Seguro', 'plan-zacatecoluca-seguro', 'plan_zacatecoluca_seguro.pdf', '2', 17, NULL, NULL, '2018-10-18 18:30:58', '2018-10-18 18:30:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(235, '2014_10_12_000000_create_users_table', 1),
(236, '2014_10_12_100000_create_password_resets_table', 1),
(237, '2017_01_23_165058_create_categorias_table', 1),
(238, '2017_01_23_165058_create_documentos_table', 1),
(239, '2017_01_23_165058_create_empresa_table', 1),
(240, '2017_01_23_165058_create_enlaces_table', 1),
(241, '2017_01_23_165058_create_equipo_table', 1),
(242, '2017_01_23_165058_create_miembro_table', 1),
(243, '2017_01_23_165058_create_miembros_documentos_table', 1),
(244, '2017_01_23_165058_create_noticias_table', 1),
(245, '2017_01_23_165058_create_servicios_table', 1),
(246, '2017_01_23_165058_create_tipos_documentos_table', 1),
(247, '2017_01_23_165058_create_tipos_enlaces_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `categoria_id` int(11) NOT NULL,
  `descripcion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contenido` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `slug`, `img`, `fecha`, `categoria_id`, `descripcion`, `contenido`, `activo`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Asamblea General de miembros CMPV.', 'asamblea-general-de-miembros-cmpv', '1.jpg', '2018-08-03', 1, 'Se sostubo una asamblea con todos lo miembros de los CMPVES.', '<p>Se sostubo una asamblea con todos lo miembros de los CMPVES.</p>', 1, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(2, 'Mesa Principal Foro Taller.', 'mesa-principal-foro-taller', '2.jpg', '2018-08-03', 1, 'Asistieron representantes de PREVENIR.GIZ, Subsecrtario de gobernabilidad Viceministerio de Prevensión social.', '<p>Asistieron representantes de PREVENIR.GIZ, Subsecrtario de gobernabilidad Viceministerio de Prevensión social.</p>', 1, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(3, 'Reunión con Instituciones Claves.', 'reunion-con-instituciones-claves', '3.jpg', '2018-08-03', 1, 'Se sostuvo una reuníón con el subgabinete nacional de prevencion, coordinadores CMPV y tecnicos PREPAZ.', '<p>Se sostuvo una reuníón con el subgabinete nacional de prevencion, coordinadores CMPV y tecnicos PREPAZ.</p>', 1, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(4, 'Diplomado.', 'diplomado', '4.jpg', '2018-08-03', 2, 'Diplomado UCA, Construccion de paz y seguridad ciudadana.', '<p>Diplomado UCA, Construccion de paz y seguridad ciudadana.</p>', 1, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(5, 'Taller de fortalecimiento.', 'taller-de-fortalecimiento', '5.jpg', '2018-08-03', 2, 'Foro taller de fortalecimiento del trabajo de articulacion interinstitucional en la implementacion del PESS, Hotel Sheraton.', '<p>Foro taller de fortalecimiento del trabajo de articulacion interinstitucional en la implementacion del PESS, Hotel Sheraton.</p>', 1, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(6, 'Gestion de Recursos.', 'gestion-de-recursos', '6.jpg', '2018-08-03', 2, 'Visita G.I.Z. gestión de asistencia para Foro-Taller.', '<p>Visita G.I.Z. gestión de asistencia para Foro-Taller.</p>', 1, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(7, 'Mi capa', 'mi-capa', 'mi_capa.jpg', NULL, 1, 'Breana Fahey DDS', '<p>asda</p>', 0, '2018-10-11 00:15:46', '2018-10-10 23:33:25', '2018-10-11 00:15:46'),
(8, 'Capacitación sobre metodologías de prevención de violencia.', 'capacitacion-sobre-metodologias-de-prevencion-de-violencia', 'capacitacion_sobre_metodologias_de_prevencion_de_violencia.jpeg', '2018-10-18', 1, 'Miembros de la REDCMPVES, en capacitación sobre metodología para la prevención de la violencia.', '<p>Miembros de la REDCMPVES, en capacitación sobre metodología para la prevención de la violencia, junto con representantes de INJUVE.</p><p>El seminario fue de dos días de duración, 17 y 18 de octubre en Hotel Mirador Plaza, San Salvador.<br></p>', 1, NULL, '2018-10-18 19:25:19', '2018-10-18 19:25:59'),
(9, 'Reciben premio por buenas prácticas de prevención de la violencia', 'reciben-premio-por-buenas-practicas-de-prevencion-de-la-violencia', 'reciben_premio_por_buenas_practicas_de_prevencion_de_la_violencia.jpg', '2018-10-22', 2, 'Coordinador de CMPV  de Armenia recibe premio', '<p>La Red Nacional de Comités Municipales de Prevención de la Violencia continúa haciendo historia, el día lunes 22 de octubre, miembros de la Red recibieron premio por buenas prácticas de prevención de la violencia implementadas en sus municipios, entre ellos el Municipio de Armenia.<br></p>', 1, NULL, '2018-10-24 00:04:14', '2018-10-24 00:05:52'),
(10, 'RedCMPVES Y LOS IMPACTOS DEL TRABAJO DE PREVENCIÓN', 'redcmpves-y-los-impactos-del-trabajo-de-prevencion', 'redcmpves_y_los_impactos_del_trabajo_de_prevencion.JPG', '2018-10-24', 2, 'El Viceministerio de Prevención y los municipios miembros de la RedCMPVES reconocen los avances en los esfuerzos de prevención de la violencia', '<p>Vea nuestro vídeo:&nbsp;<a target=\"_blank\" rel=\"nofollow\" href=\"https://www.facebook.com/RedCMPVES/videos/246710302870244/\">https://www.facebook.com/RedCMPVES/videos/246710302870244/</a></p>', 1, NULL, '2018-10-25 22:12:50', '2019-09-14 02:52:56'),
(11, 'RedCMPVES analiza anteproyecto de ley SINAPREV', 'redcmpves-analiza-anteproyecto-de-ley-sinaprev', 'redcmpves_analiza_anteproyecto_de_ley_sinaprev.JPG', '2018-10-29', 3, 'Miembros de la RedCMPVES se reunieron para conocer y analizar el anteproyecto de ley SINAPREV', '<p>Miembros de la Red de Comités Municipales de Prevención de la Violencia de El Salvador se reunieron el día miércoles 24 de octubre para conocer y analizar el anteproyecto de ley del Sistema Nacional de Prevención de la Violencia de El Salvador.</p><p>Una de sus principales observaciones está puesta en cuanto al financiamiento que la ley debe prever para que todos los actores involucrados en el trabajo de prevención puedan tener éxito.</p><p><br></p>', 1, NULL, '2018-10-26 00:54:04', '2018-11-29 02:16:06'),
(12, 'Red de Comités Municipales de Prevención de la Violencia Consideran  Necesaria Aprobación de una Ley de Prevención', 'red-de-comites-municipales-de-prevencion-de-la-violencia-consideran-necesaria-aprobacion-de-una-ley-de-prevencion', 'red_de_comites_municipales_de_prevencion_de_la_violencia_consideran_necesaria_aprobacion_de_una_ley_de_prevencion.jpeg', '2018-11-11', 3, 'Viceministro de Prevención Social, Subsecretario de Gobernabilidad, Diputados, Alcaldes y Miembros de la Red se dieron cita para avanzar hacia la aprobación del SINAPREV', '<p>Miembros de la Red Nacional de Comités Municipales de Prevención de la Violencia (REDCMPVES), urgieron a diputados de la Comisión de Seguridad y Combate a la Narcoactividad de la Asamblea Legislativa, la aprobación de la Ley del Sistema Nacional de Prevención de la Violencia, anteproyecto que fue presentado por el Ejecutivo al parlamento en el mes de junio de 2017.</p><p></p><p>La REDCMPVES entregó a los legisladores algunas observaciones que buscan enriquecer el contenido del documento presentado a la Asamblea Legislativa, donde se destaca la necesidad de que la Ley garantice los mecanismos para el financiamiento en la implementación de las acciones de prevención a nivel local; así como la articulación entre actores nacionales y locales.</p><p></p><p>El anteproyecto de Ley del Sistema Nacional de Prevención de la violencia busca que la prevención de la violencia esté establecido en una ley para que sea de obligatorio cumplimiento tanto de los municipios como en el Gobierno central independientemente de quienes gobiernen cada municipio y a nivel central.</p>\r\n\r\n<p>La Red es una instancia que aglutina a 23 Comités Municipales de Prevención de la Violencia, de igual número de municipios contemplados dentro del Plan El Salvador Seguro; promueve y busca fortalecer el trabajo articulado interinstitucional que realizan los actores locales y regionales en materia de prevención de la violencia y construcción de cultura de paz, con el objeto de disminuir los índices de inseguridad en los municipios.<br></p>\r\n\r\n<p>Los planteamientos de la RED fueron realizados en un desayuno organizado con el apoyo de la Agencia de Cooperación Internacional Alemana, a través de GIZ-PREVENIR, en coordinación con el Viceministerio de Prevención Social y la asistencia técnica de la Fundación de Apoyo a Municipios de El Salvador (FUNDAMUNI). El cual contó con la presencia de diputados, alcaldes y funcionarios de gobierno.<br></p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n<br><p></p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n<br><p>&nbsp;</p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n<p></p>', 1, NULL, '2018-11-09 09:09:28', '2019-02-19 01:18:11'),
(13, 'Reunion de planificacion y coordinacion de ETC de la RedCMPVES.', 'reunion-de-planificacion-y-coordinacion-de-etc-de-la-redcmpves', 'reunion_de_planificacion_y_coordinacion_de_etc_de_la_redcmpves.jpeg', '2019-03-13', 2, 'El pasado miercoles 13 de marzo del 2019, se llevo a cabo una reunion de planificacion y coordinacion, con miras a la primera Asamblea General de la Red, donde se validara el POA 2019.', '<p><img alt=\"\" src=\"http://reunion \"><br></p>', 1, NULL, '2019-03-17 02:31:16', '2019-06-22 02:07:10'),
(14, 'Reunión extraordinaria Equipo Técnico coordinador. ETC', 'reunion-extraordinaria-equipo-tecnico-coordinador-etc', 'reunion_extraordinaria_equipo_tecnico_coordinador_etc.jpeg', '2019-09-14', 1, 'Seguimiento hoja de Ruta RedCMPVES 2019.', '<blockquote><p>Con la finalidad de dar seguimiento a la hoja de ruta de la Red, miembros del Equipo Técnico sostuvieron una reunion extraordinaria para revisar avances y definir estrategias ante la nueva realidad política actual. CAbe resaltar&nbsp;que se re-estructuró dicho Comité.</p></blockquote>', 1, NULL, '2019-09-14 02:08:42', '2019-09-14 02:45:40'),
(15, 'Asamblea General RedCMPVES', 'asamblea-general-redcmpves', 'asamblea_general_redcmpves.jpeg', '2019-10-12', 1, 'Seguimiento hoja de Ruta RedCMPVES 2019.', '<p>En orden de dar continuidad a la hoja de ruta en incidencia de la Red Nacional de CMPV de El Salvador, se desarrollo Asamblea General extraordinaria, en las nuevas oficinas de la Fundación de Apoyo a Municipios de El Salvador, FUNDAMUNI, Para para recibir visita del Director Ejecutivo de la Organización No Gubernamental <b>JUNIOR ACHIEVMENT DE EL SALVADOR</b>, <u>Lic. Ricardo Monterrosa</u>, acompañado de <u>Licda. Karen Lopez</u> , encargada de RRPP, quien presentó a las y los Coordinadores de CMPV, la plataforma de programas enfocados al Desarrollo integral de la niñez y juventud, entre ellos: Educación financiera,&nbsp; Inteligencia emocional y Mujeres emprendedoras. Oportuno mencionar <i><b>el agradecimiento especia</b></i>l a JA por la <u>donación de 20 Becas para jóvenes, y un TV HD</u>, para fortalecimiento de la RedCMPVES.</p>', 1, NULL, '2019-10-12 04:16:34', '2019-10-12 04:21:09'),
(16, 'Fortalecimiento a la RedCMPVES con camisas  logotipo Polo', 'fortalecimiento-a-la-redcmpves-con-camisas-logotipo-polo', 'fortalecimiento_a_la_redcmpves_con_camisas_con_logotipo_polo.jpeg', '2019-10-12', 1, 'Seguimiento hoja de Ruta RedCMPVES 2019.', '<p>Con enorme <b>agradecimiento</b> a la municipalidad que preside el <u>Sr. Alcalde de Santo Tomás,&nbsp; Lic. Efraín Cañas</u>,&nbsp; miembros de la RedCMPVES, recibieron durante la Asamblea General extra-ordinaria, un fortalecimiento en imagen, consistente en camisas Polo con logotipo de la misma, para utilizarlas en eventos a los que se invita a la Red.&nbsp; La imagen muestra la satisfacción de las y los coordinadores CMPV,&nbsp; al recibir la dotación, cuya gestión estuvo a cargo de Lic. Julio Melara, Coordinador del CMPV de Santo Tomás.&nbsp;&nbsp;</p>', 1, NULL, '2019-10-12 04:28:57', '2019-10-12 04:29:52'),
(17, 'CON PROFUNDA PENA Y DOLOR LOS MIEMBROS DE LA REDCMPEVES NOS UNIMOS A LA FAMILIA DE DON MANUELITO', 'con-profunda-pena-y-dolor-los-miembros-de-la-redcmpeves-nos-unimos-a-la-familia-de-don-manuelito', NULL, '2020-08-31', 1, 'Muere un amigo y hermano', '<p>Los miembros de la Red de Comités Municipales de Prevención de la Violencia de El Salvador, lamentamos el fallecimiento de nuestro querido compañero y amigo Manuel Henríquez, coordinador del CMPV del municipio de Ilobasco.&nbsp;</p><p>Siempre te recordaremos como la persona alegre y jovial, propositiva, comprometida y leal. Que el Buen Padre Dios te reciba en el cielo.</p><p>Oramos para que Dios fortalezca a su familia y le expresamos nuestras&nbsp;profundas condolencia.</p><p>Hasta siempre querido amigo!</p>', 0, NULL, '2020-08-31 09:48:58', '2021-02-02 01:20:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `contenido` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`id`, `nombre`, `img`, `descripcion`, `contenido`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Fortalecimiento Institucional', 'instituciones.jpg', 'Se busca la coordinación con actores nacionales, públicos y privados, la gestión de recursos técnicos y financieros, el fortalecimiento de habilidades técnico-metodológicas y el intercambio de experiencias.', NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(2, 'Incidencia Política', 'politica.jpg', 'Buscamos la visibilización y el posicionamiento de las acciones realizadas por los CMPVES, la formulación de leyes, planes, programas, y proyectos nacionales en prevención de la violencia.', NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(3, 'Gestión de Recursos', 'recursos.jpg', 'Buscamos gestionar cooperaciones ante instancias públicas y privadas, nacionales e internacionales para el fortalecimiento de la Red y la ejecución de proyectos locales y regionales.', NULL, NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_documentos`
--

CREATE TABLE `tipo_documentos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tipo_documentos`
--

INSERT INTO `tipo_documentos` (`id`, `nombre`, `slug`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Marco normativo', 'marco-normativo', NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(2, 'Diagnósticos y planes de prevención', 'diagnosticos-y-planes-de-prevencion', NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(3, 'Herramientas para la prevención', 'herramientas-para-la-prevencion', NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(4, 'Buenas prácticas', 'buenas-practicas', NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_enlaces`
--

CREATE TABLE `tipo_enlaces` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tipo_enlaces`
--

INSERT INTO `tipo_enlaces` (`id`, `nombre`, `slug`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Estadísticas', 'estadisticas', NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(2, 'Oportunidades', 'oportunidades', NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(3, 'Institucionales', 'institucionales', NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(4, 'Otro', 'otro', NULL, '2018-10-10 23:11:48', '2018-10-10 23:11:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Wigberto Castillo', 'coordinadorcmpvecojute@gmail.com', '$2y$10$Qxx6Xbfl8j2mqa8GYjrGxu8VDltpcp021VDNddLlK5TzXk7C06A/a', 'su9LLtS7nIEtvswLmdBE3FRk87Z5bxyyLjz2CbWBRJrxVSNwTIZyYHRWyx6J', '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(2, 'Jesus Alvarado', 'alvarado.websis@gmail.com', '$2y$10$T8K9QM0vFl1zW8R.fKoeA.OhEW4SGX7WKRF1qpRQMYmqviiIQa4K.', 'yI82RlgXdG8mf4uaW93DcSu6rY0lzqsQZ37uygIe1y4sRern4Xx1Rdwibauq', '2018-10-10 23:11:48', '2018-10-10 23:11:48'),
(3, 'Edgardo Ramírez', 'edgardo.fundamuni@gmail.com', '$2y$10$et0z2SbGmls5u2KW8cxiY.595lGQc/bFJKieNP/O99ABqc4dq7emC', 'rRFpMzPxkt9wfBTvxW3jOheRdqfMBIO5pqWTLNIDNWR4byl3bjg51AplZLKj', NULL, NULL),
(4, 'Debbie Moran', 'cmpv.alcaldiass@gmail.com', '$2y$10$LLzIKY8PoiRDYs48ZAH51uKYR5Zf6clN/bOR/o5adBF3FbnejscGu', NULL, '2021-02-02 01:04:45', '2021-02-02 01:04:45');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `documentos`
--
ALTER TABLE `documentos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `documentos_slug_unique` (`slug`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `enlaces`
--
ALTER TABLE `enlaces`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `miembros`
--
ALTER TABLE `miembros`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `miembros_slug_unique` (`slug`);

--
-- Indices de la tabla `miembros_documentos`
--
ALTER TABLE `miembros_documentos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `miembros_documentos_slug_unique` (`slug`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `noticias_slug_unique` (`slug`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_documentos`
--
ALTER TABLE `tipo_documentos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tipo_documentos_slug_unique` (`slug`);

--
-- Indices de la tabla `tipo_enlaces`
--
ALTER TABLE `tipo_enlaces`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tipo_enlaces_slug_unique` (`slug`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `documentos`
--
ALTER TABLE `documentos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `enlaces`
--
ALTER TABLE `enlaces`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `equipos`
--
ALTER TABLE `equipos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `miembros`
--
ALTER TABLE `miembros`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `miembros_documentos`
--
ALTER TABLE `miembros_documentos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=248;

--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `servicios`
--
ALTER TABLE `servicios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo_documentos`
--
ALTER TABLE `tipo_documentos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tipo_enlaces`
--
ALTER TABLE `tipo_enlaces`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
