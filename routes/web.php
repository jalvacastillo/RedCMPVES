<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 							'HomeController@index'			)->name('index');
Route::get('/nosotros', 					'NosotrosController@index'		)->name('nosotros');

Route::get('/conocimientos', 				'ConocimientosController@index'	)->name('conocimientos');
Route::get('/conocimientos/{tipo}', 		'ConocimientosController@tipo'	)->name('conocimiento');
Route::post('/conocimientos', 				'ConocimientosController@buscar')->name('conocimiento-buscar');

Route::get('/enlaces', 						'EnlacesController@index'		)->name('enlaces');
Route::get('/enlaces/{tipo}',				'EnlacesController@tipo'		)->name('enlace');
Route::post('/enlaces', 					'EnlacesController@buscar'		)->name('enlace-buscar');

Route::get('/nuestra-red', 					'MiembrosController@index'		)->name('red');
Route::get('/nuestra-red/miembro/{slug}',	'MiembrosController@miembro'	)->name('miembro');

Route::get('/noticias',                     'NoticiasController@index'      )->name('noticias');
Route::get('/noticias/{tipo}', 			    'NoticiasController@tipo'		)->name('noticiaTipo');
Route::get('/noticia/{slug}', 				'NoticiasController@noticia'	)->name('noticia');
Route::get('/contactos', 					'ContactosController@index'		)->name('contactos');
Route::post('/correo', 						'ContactosController@correo'	)->name('correo');

require base_path('routes/dash.php');
