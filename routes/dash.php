<?php

Auth::routes();


Route::group(array('prefix' => 'admin', 'middleware' => 'auth'), function(){
    
    Route::get('/', 				    'Admin\DashboardController@index');
    Route::get('/noticias', 		    'Admin\NoticiasController@index');
    Route::get('/noticia/{slug}', 		'Admin\NoticiasController@noticia');
    Route::post('/noticia/{slug}',      'Admin\NoticiasController@guardar');
    Route::get('/noticia/delete/{id}',  'Admin\NoticiasController@delete');
    Route::get('/noticias/buscar',      'Admin\NoticiasController@buscar');

    Route::get('/categorias',           'Admin\CategoriaController@index');
    Route::get('/categoria/{id}',       'Admin\CategoriaController@categoria');
    Route::post('/categoria/{id}',      'Admin\CategoriaController@guardar');

    Route::get('/miembros',               'Admin\MiembrosController@index');
    Route::get('/miembro/{id}',           'Admin\MiembrosController@miembro');
    Route::post('/miembro/{id}',          'Admin\MiembrosController@guardar');
    Route::get('/miembro/delete/{id}',    'Admin\MiembrosController@delete');
    Route::get('/miembros/{miembro}/doc/{id}',    'Admin\MiembrosController@documento');
    Route::post('/miembros/{miembro}/doc/{id}',    'Admin\MiembrosController@guardarDocumento');

    Route::post('/miembros/documento', 'Admin\MiembrosController@guardarDocumento');
    Route::get('/miembro/documento/{id}', 'Admin\MiembrosController@eliminarDocumento');

    Route::get('/documentos',            'Admin\DocumentosController@index');
    Route::get('/documento/{slug}',      'Admin\DocumentosController@documento');
    Route::post('/documento/{slug}',     'Admin\DocumentosController@guardar');
    Route::get('/documento/delete/{id}', 'Admin\DocumentosController@delete');
    Route::get('/documentos/buscar',      'Admin\DocumentosController@buscar');

    Route::get('/enlaces',            'Admin\EnlacesController@index');
    Route::get('/enlace/{slug}',      'Admin\EnlacesController@enlace');
    Route::post('/enlace/{slug}',     'Admin\EnlacesController@guardar');
    Route::get('/enlace/delete/{id}', 'Admin\EnlacesController@delete');
    Route::get('/enlaces/buscar',      'Admin\EnlacesController@buscar');

    Route::get('/correo', 			function(){ return view('dash.views.mail.compose');});


    Route::get('/usuarios/auth', 	'Admin\UsuariosController@auth');

});