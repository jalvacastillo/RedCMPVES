@extends('admin.app')

@section('content')
<div class="container bg-light">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Hola <b>{{ Auth::user()->name }}</b>!
                </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif


                    <div class="col-xs-6">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h2>Noticias</h2>
                                <h3>{{ $noticias }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h2>CMPVES</h2>
                                <h3>{{ $redes }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h2>Documentos</h2>
                                <h3>{{ $documentos }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h2>Enlaces</h2>
                                <h3>{{ $enlaces }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
