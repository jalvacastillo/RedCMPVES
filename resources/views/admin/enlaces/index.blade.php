@extends('admin.app')

@section('title')
    Enlaces
@endsection

@section('content')

    <section class="container">


        <div class="panel panel-default">
        
            <div class="panel-header">
              <h3 class="panel-heading pull-left">Enlaces <small class="badge badge-info">{{ $enlaces->total() }}</small></h3>
              <form action="/admin/enlaces/buscar" method="GET">
              <div class="panel-tools">
                <div class="input-group pull-right">
                  <input type="text" name="nombre" class="form-control input-sm pull-right" placeholder="Buscar">
                  <div class="input-group-btn">
                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                    <a href="{{ url('admin/enlace/nuevo') }}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></a>
                    {{-- <a data-toggle="modal" href='#modal-docs' class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></a> --}}
                  </div>
                </div>
              </div>
            </form>
              <br>
            </div>

            <div class="panel-body">
                <table class="table table-bordered table-hover table-responsive">
                    <thead>
                        <tr>
                            <th>Subido</th>
                            <th>Nombre</th>
                            <th>Categoria</th>
                            <th class="text-center">Ver</th>
                            <th class="text-center"><i class="fa fa-cog"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($enlaces as $enlace)
                        <tr>
                            <td>{{ $enlace->created_at->diffForHumans() }}</td>
                            <td>{{ str_limit($enlace->nombre, 50, '...') }} </td>
                            <td>{{ $enlace->tipo()->first()->nombre}} </td>
                            <td class="text-center"><a href="{{ $enlace->url }}" target="_black"><i class="fa fa-link fa-2x"></i></a></td>
                            <td class="text-center">
                                <div class="btn-group" role="group">
                                    <a href="{{ url('admin/enlace', $enlace->id) }}" type="button" class="btn btn-default btn-sm" title="Actualizar">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a onclick="return confirm('Confirma eliminar el registro?')" href="{{ url('admin/enlace/delete', $enlace->id) }}" class="btn btn-default btn-sm" title="Eliminar">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="panel-footer text-center">
                {{ $enlaces->links() }}
            </div>

        </div>

    </section>

    <div class="modal fade" id="modal-docs">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ url('admin/enlaces/subir') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Documento</h4>
                </div>
                <div class="modal-body">
                        {{ csrf_field() }}
                        
                        <div class="form-group">
                            <label for="nombre">Nombre:</label>
                            <input type="text" class="form-control" name="nombre" required>
                        </div>
                        <div class="form-group">
                            <label for="tipo">Tipo:</label>
                            <select name="tipo_id" class="form-control" required>
                              <option value="1">Marco normativo</option>
                              <option value="2">Diagnósticos y planes de prevención</option>
                              <option value="3">Herramientas para la prevención</option>
                              <option value="4">Buenas prácticas</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="tipo">Documento:</label>
                            <input type="file" name="file" class="form-control" accept="application/pdf" required>
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
                </form>
            </div>
        </div>
    </div>

@endsection

