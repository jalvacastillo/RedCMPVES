@extends('admin.app')

@section('title')
    Enlace
@endsection

@section('content')

    <section class="container">
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

    <form method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="panel panel-default">
        <div class="panel-body">
            
            <input type="hidden" name="id" value="{{$enlace->id}}">

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="control-label" for="nombre"> * Nombre:</label>
                    <div class="controls">
                         <input type="text" value="{{$enlace->nombre}}" class="form-control" name="nombre" placeholder="Nombre" required />
                    </div>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="control-label" for="url"> * URL:</label>
                    <div class="controls">
                         <input type="url" value="{{$enlace->url}}" class="form-control" name="url" placeholder="url" required />
                    </div>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="control-label" for="categoria"> * Tipo:</label>
                    <div class="controls">
                        <select class="form-control" name="tipo_id" value="{{$enlace->tipo_id}}">
                            @foreach ($tipos as $tipo)
                                <option value="{{$tipo->id}}" 
                                    {{ $enlace->tipo_id == $tipo->id ? 'selected' : '' }}>
                                    {{$tipo->nombre}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

        </div>
        <div class="panel-footer">
            <div class="row">
                <button type="submit" class="btn btn-primary pull-right">Guardar</button>
            </div>
        </div>
    </form>

    </section>

@endsection