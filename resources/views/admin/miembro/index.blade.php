@extends('admin.app')

@section('title')
    CMPVES
@endsection

@section('content')

    <section class="container">


        <div class="panel panel-default">
        
            <div class="panel-header">
              <h3 class="panel-heading pull-left">CMPVES</h3>
              <div class="panel-tools">
                <div class="input-group">
                  <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Buscar">
                  <div class="input-group-btn">
                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                    <a href="{{ url('admin/cmpve/nuevo') }}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></a>
                  </div>
                </div>
              </div>
              <br>
            </div>

            <div class="panel-body">
                <table class="table table-bordered table-hover table-responsive">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Departamento</th>
                            <th class="text-center">Imagen</th>
                            <th class="text-center">Documentos</th>
                            <th class="text-center"><i class="fa fa-cog"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($redes as $red)
                        <tr>
                            <td>{{ $red->id}} </td>
                            <td>{{ $red->nombre }} </td>
                            <td>{{ $red->departamento }} </td>
                            <td class="text-center"><img src="{{ asset('storage/red/'.$red->img) }}" height="30px;"> </td>
                            <td class="text-center">{{ $red->documentos()->count() == 0 ? 'Ninguno' : $red->documentos()->count()}} </td>
                            <td class="text-center">
                                <div class="btn-group" role="group">
                                    <a href="{{ url('admin/miembro', $red->id) }}" type="button" class="btn btn-default btn-sm" title="Actualizar">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a onclick="return confirm('Confirma eliminar el registro?')" href="{{ url('admin/miembro/delete', $red->id) }}" class="btn btn-default btn-sm" title="Eliminar">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            {{-- <div class="panel-footer clearfix">
                {{ $redes->links() }}
            </div> --}}

        </div>

    </section>

@endsection

