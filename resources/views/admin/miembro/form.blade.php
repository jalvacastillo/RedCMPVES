@extends('admin.app')

@section('title')
    CMPVE
@endsection

@section('content')

    <section class="container">
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

    <form method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="panel panel-default">
        <div class="panel-body">
            
            <input type="hidden" name="id" value="{{$miembro->id}}">
            {{-- Imagen --}}
            <div class="col-xs-12 text-center">
                @if ($miembro->img)
                    <img src="{{ asset('storage/red/'.$miembro->img) }}" alt="{{ $miembro->nombre}}" height="100px;">
                @endif  
            </div>
            <div class="col-xs-12 text-center">
                <div class="form-group">
                    <input type="file" name="file">
                    <input type="hidden" name="img" value="{{$miembro->img}}">
                </div>
            </div>
            {{-- Titulo --}}
            <div class="col-xs-4">
                <div class="form-group">
                    <label class="control-label" for="nombre"> * Nombre:</label>
                    <div class="controls">
                         <input type="text" value="{{$miembro->nombre}}" class="form-control" name="nombre" placeholder="Titulo" required />
                    </div>
                </div>
            </div>

            <div class="col-xs-4">
                <div class="form-group">
                    <label class="control-label" for="telefono">Teléfono:</label>
                    <div class="controls">
                        <div class="input-group">
                            <span class="input-group-addon">+503</span>
                            <input type="text" value="{{$miembro->telefono}}" class="form-control" name="telefono" placeholder="Telefono"/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-4">
                <div class="form-group">
                    <label class="control-label" for="correo">Correo:</label>
                    <div class="controls">
                        <input type="email" value="{{$miembro->correo}}" class="form-control" name="correo" placeholder="Correo"/>
                    </div>
                </div>
            </div>

            <div class="col-xs-4">
                <div class="form-group">
                    <label class="control-label" for="url_facebook">FanPage:</label>
                    <div class="controls">
                        <input type="text" value="{{$miembro->url_facebook}}" class="form-control" name="url_facebook" placeholder="Facebook"/>
                    </div>
                </div>
            </div>

            <div class="col-xs-4">
                <div class="form-group">
                    <label class="control-label" for="web">Web:</label>
                    <div class="controls">
                        <input type="text" value="{{$miembro->web}}" class="form-control" name="web" placeholder="Página web"/>
                    </div>
                </div>
            </div>

            <div class="col-xs-4">
                <div class="form-group">
                    <label class="control-label" for="coords">Coordenadas:</label>
                    <div class="controls">
                        <input type="text" value="{{$miembro->coords}}" class="form-control" name="coords" placeholder="Coordenadas"/>
                    </div>
                </div>
            </div>

            <div class="col-xs-3">
                <div class="form-group">
                    <label class="control-label" for="lat">Latitud:</label>
                    <div class="controls">
                        <input type="text" value="{{$miembro->lat}}" class="form-control" name="lat" placeholder="Latitud"/>
                    </div>
                </div>
            </div>

            <div class="col-xs-3">
                <div class="form-group">
                    <label class="control-label" for="lon">Longitud:</label>
                    <div class="controls">
                        <input type="text" value="{{$miembro->lon}}" class="form-control" name="lon" placeholder="Longitud"/>
                    </div>
                </div>
            </div>

            <div class="col-xs-3">
                <div class="form-group">
                    <label class="control-label" for="departamento">Departamento:</label>
                    <div class="controls">
                        <input type="text" value="{{$miembro->departamento}}" class="form-control" name="departamento" placeholder="Departamento"/>
                    </div>
                </div>
            </div>



            <div class="col-xs-3">
                <div class="form-group">
                    <label class="control-label" for="direccion">Dirección:</label>
                    <div class="controls">
                        <input type="text" value="{{$miembro->direccion}}" class="form-control" name="direccion" placeholder="Dirección"/>
                    </div>
                </div>
            </div>

        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary pull-right">Guardar</button>
                </div>
            </div>
        </div>
    </form>

    </section>

    <section class="container">
        <div class="panel panel-default">
            <div class="panel-heading" style="height: 60px;">
                <h3 class="pull-left" style="margin: 5px 0px;">Documentos <small class="badge badge-info">{{ $miembro->documentos()->count() }}</small></h3>
                <a data-toggle="modal" href='#modal-doc' class="btn btn-primary pull-right"><i class="fa fa-plus"></i></a>
            </div>
            <div class="panel-body">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center"><i class="fa fa-cog"></i></th>
                            <th>Nombre</th>
                            <th>Tipo</th>
                            <th class="text-center">Ver</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($miembro->documentos as $doc)
                        <tr>
                            <td class="text-center">
                                <a onclick="return confirm('Confirma eliminar el registro?')" href="{{ url('admin/miembro/documento', $doc->id) }}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                <a href="{{ url('/admin/miembros/'.$miembro->slug.'/doc/'.$doc->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                            </td>
                            <td>{{$doc->nombre}}</td>
                            <td>{{$doc->tipo()->first()->nombre}}</td>
                            <td class="text-center"><a href="{{ asset('storage/documentos/miembros/'.$doc->url) }}" target="_black"><i class="fa fa-file-pdf-o fa-2x"></i></a></td>
                        </tr>
                        @endforeach
                        @if ($miembro->documentos()->count() <= 0)
                            <tr>
                                <td colspan="5"><p class="text-center">No se han subido documentos</p></td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <div class="modal fade" id="modal-doc">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ url('admin/miembros/documento') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Documento</h4>
                </div>
                <div class="modal-body">
                        {{ csrf_field() }}

                        <input type="hidden" name="miembro_id" value="{{$miembro->id}}">
                        <input type="hidden" name="miembro_slug" value="{{$miembro->slug}}">
                        
                        <div class="form-group">
                            <label for="tipo">Documento:</label>
                            <input type="file" name="file" class="form-control" accept="application/pdf" required>
                        </div>

                        <div class="form-group">
                            <label for="nombre">Nombre:</label>
                            <input type="text" class="form-control" name="nombre" required>
                        </div>

                        <div class="form-group">
                            <label for="tipo">Tipo:</label>
                            <select name="tipo_id" class="form-control" required>
                              <option value="1">Marco normativo</option>
                              <option value="2">Diagnósticos y planes de prevención</option>
                              <option value="3">Herramientas para la prevención</option>
                              <option value="4">Buenas prácticas</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="descripcion"> Descripción:</label>
                            <div class="controls">
                                <textarea name="descripcion" class="form-control" cols="30" rows="3"></textarea>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
                </form>
            </div>
        </div>
    </div>

@endsection