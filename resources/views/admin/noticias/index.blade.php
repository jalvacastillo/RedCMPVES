@extends('admin.app')

@section('title')
    Noticias
@endsection

@section('content')

    <section class="container">


        <div class="panel panel-default">
        
            <div class="panel-header">
              <h3 class="panel-heading pull-left">Noticias</h3>
              <form action="/admin/noticias/buscar" method="GET">
              <div class="panel-tools">
                <div class="input-group pull-right">
                  <input type="text" name="nombre" class="form-control input-sm pull-right" placeholder="Buscar">
                  <div class="input-group-btn">
                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                    <a href="{{ url('admin/noticia/nuevo') }}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></a>
                  </div>
                </div>
              </div>
          </form>
              <br>
            </div>

            <div class="panel-body">
                <table class="table table-bordered table-hover table-responsive">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Publicado</th>
                            <th>Titulo</th>
                            <th>Categoria</th>
                            <th class="text-center">Activo</th>
                            <th class="text-center"><i class="fa fa-cog"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($noticias as $noticia)
                        <tr>
                            <td>{{ $noticia->id}} </td>
                            <td>{{ $noticia->created_at->diffForHumans() }}</td>
                            <td>{{ str_limit($noticia->titulo, 50, '...') }} </td>
                            <td>{{ $noticia->categoria()->first()->nombre}} </td>
                            <td class="text-center">
                                <form action="{{ url('/admin/noticia/'. $noticia->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{$noticia->id}}">
                                    <input type="hidden" name="titulo" value="{{$noticia->titulo}}">
                                    <input type="hidden" name="descripcion" value="{{$noticia->descripcion}}">
                                    <input type="hidden" name="categoria_id" value="{{$noticia->categoria_id}}">
                                    <input type="hidden" name="contenido" value="{{$noticia->contenido}}">
                                    <input type="checkbox" name="activo" value="1" {{ ($noticia->activo) == 1 ? 'checked' : '' }} onchange="document.getElementById('submit').click()">
                                    <input type="submit" id="submit" style="display: none;">
                                </form>
                            </td>
                            <td class="text-center">
                                <div class="btn-group" role="group">
                                    <a href="{{ url('admin/noticia', $noticia->id) }}" type="button" class="btn btn-default btn-sm" title="Actualizar">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a onclick="return confirm('Confirma eliminar el registro?')" href="{{ url('admin/noticia/delete', $noticia->id) }}" class="btn btn-default btn-sm" title="Eliminar">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="panel-footer clearfix">
                {{ $noticias->links() }}
            </div>

        </div>

    </section>

@endsection

