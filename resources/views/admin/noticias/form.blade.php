@extends('admin.app')

@section('title')
    Noticia
@endsection

@section('css')

  <link rel="stylesheet" href="{{ asset('dash/css/bootstrap-editor.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dash/css/bootstrap-tags.css') }}">

@endsection

@section('content')

    <section class="container">
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

    <form method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="panel panel-default">
        <div class="panel-body">
            
            <input type="hidden" name="id" value="{{$noticia->id}}">
            {{-- Imagen --}}
            <div class="col-xs-6 text-center">
                <div class="form-group">
                    <input type="file" name="file">
                    <input type="hidden" name="img" value="{{$noticia->img}}">
                </div>
            </div>
            <div class="col-xs-6 text-center">
                @if ($noticia->img)
                    <img src="{{ asset('storage/noticias/'.$noticia->img) }}" alt="{{ $noticia->titulo}}" height="100px;">
                @endif  
            </div>
            {{-- Titulo --}}
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="control-label" for="fecha"> * Fecha:</label>
                    <div class="controls">
                         <input type="date" value="{{$noticia->fecha}}" class="form-control" name="fecha" placeholder="Fecha" required />
                    </div>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="control-label" for="titulo"> * Titulo:</label>
                    <div class="controls">
                         <input type="text" value="{{$noticia->titulo}}" class="form-control" name="titulo" placeholder="Titulo" required />
                    </div>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="control-label" for="descripcion"> * Descripción:</label>
                    <div class="controls">
                        <input type="text" value="{{$noticia->descripcion}}" class="form-control" name="descripcion" placeholder="Descripción" required />
                    </div>
                </div>
            </div>

            <div class="col-xs-6">
                <div class="form-group">
                    <label class="control-label" for="categoria"> * Categoria:</label>
                    <div class="controls">
                        <select class="form-control" name="categoria_id" value="{{$noticia->categoria_id}}">
                            @foreach ($categorias as $cat)
                                <option value="{{$cat->id}}" 
                                    {{ $noticia->categoria_id == $cat->id ? 'selected' : '' }}>
                                    {{$cat->nombre}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-xs-6">
                <div class="form-group">
                    <label class="control-label" for="activo"> * Activo:</label>
                    <div class="controls">
                        <input type="checkbox" value="{{$noticia->activo}}" name="activo" {{ ($noticia->activo) == 1 ? 'checked' : '' }} />
                    </div>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="control-label" for="telefono"> Contenido:</label>
                    <div class="controls">
                        <textarea id="compose-textarea" name="contenido" class="form-control" style="height: 400px">{{$noticia->contenido}}</textarea>
                    </div>
                </div>
            </div>

        </div>
        <div class="panel-footer">
            <div class="row">
                <button type="submit" class="btn btn-primary pull-right">Guardar</button>
            </div>
        </div>
    </form>

    </section>

@endsection

@section('js')

  <script src="{{ asset('dash/js/bootstrap-editor.min.js') }}"></script>
  <script src="{{ asset('dash/js/bootstrap-tags.js') }}"></script>

  <script>
    $(function () {
      //Add text editor
      $("#compose-textarea").wysihtml5();

    });
  </script>

@endsection