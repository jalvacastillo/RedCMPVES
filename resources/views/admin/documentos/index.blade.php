@extends('admin.app')

@section('title')
    Documentos
@endsection

@section('content')

    <section class="container">


        <div class="panel panel-default">
        
            <div class="panel-header">
              <h3 class="panel-heading pull-left">Documentos <small class="badge badge-info">{{ $documentos->total() }}</small></h3>
                <form action="/admin/documentos/buscar" method="GET">                        
              <div class="panel-tools">
                <div class="input-group pull-right">
                      <input type="text" name="nombre" class="form-control input-sm pull-right" placeholder="Buscar">
                      <div class="input-group-btn">
                        <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                        <a href="{{ url('admin/documento/nuevo') }}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></a>
                      </div>
                </div>
              </div>
                </form>
              <br>
            </div>

            <div class="panel-body">
                <table class="table table-bordered table-hover table-responsive">
                    <thead>
                        <tr>
                            <th>Subido</th>
                            <th>Nombre</th>
                            <th>Categoria</th>
                            <th class="text-center">Ver</th>
                            <th class="text-center"><i class="fa fa-cog"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($documentos as $documento)
                        <tr>
                            <td>{{ $documento->created_at->diffForHumans() }}</td>
                            <td>{{ str_limit($documento->nombre, 50, '...') }} </td>
                            <td>{{ $documento->tipo()->first()->nombre}} </td>
                            <td class="text-center"><a href="{{ asset('storage/'. $documento->url) }}" target="_black"><i class="fa fa-file-pdf-o fa-2x"></i></a></td>
                            <td class="text-center">
                                <div class="btn-group" role="group">
                                    <a href="{{ url('admin/documento', $documento->id) }}" type="button" class="btn btn-default btn-sm" title="Actualizar">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a onclick="return confirm('Confirma eliminar el registro?')" href="{{ url('admin/documento/delete', $documento->id) }}" class="btn btn-default btn-sm" title="Eliminar">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="panel-footer text-center">
                {{ $documentos->links() }}
            </div>

        </div>

    </section>

@endsection

