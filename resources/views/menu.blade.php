<nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar" id="navbar">
  <div class="container"><a class="navbar-brand" href="{{ route('index') }}"><strong>RedCMPVES</strong></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
    <div class="collapse navbar-collapse" id="navbarContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link" href="{{ route('nosotros') }}">
            Nosotros
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="{{ url('/nosotros#somos') }}">Quienes somos</a>
            <a class="dropdown-item" href="{{ url('/nosotros#naturaleza') }}">Naturaleza</a>
            <a class="dropdown-item" href="{{ url('/nosotros#naturaleza') }}">Objetivo</a>
            <a class="dropdown-item" href="{{ url('/nosotros#principios') }}">Principios</a>
            <a class="dropdown-item" href="{{ url('/nosotros#equipo') }}">Equipo</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" href="{{ route('conocimientos') }}">
            Gestión del Conocimiento
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="{{ route('conocimiento', 'marco-normativo') }}">Marco normativo</a>
            <a class="dropdown-item" href="{{ route('conocimiento', 'diagnosticos-y-planes-de-prevencion') }}">Diagnósticos y planes de prevención</a>
            <a class="dropdown-item" href="{{ route('conocimiento', 'herramientas-para-la-prevencion') }}">Herramientas para la prevención</a>
            <a class="dropdown-item" href="{{ route('conocimiento', 'buenas-practicas') }}">Buenas prácticas</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" href="{{ route('red') }}">
            Nuestra Red
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="{{ url('/nuestra-red#ejes-tematicos') }}">Ejes Temáticos</a>
            <a class="dropdown-item" href="{{ url('/nuestra-red#cmpves') }}">CMPVES</a>
            <a class="dropdown-item" href="{{ url('/nuestra-red#resultados') }}">Resultados</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" href="{{ route('noticias') }}">
            Noticias
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="{{ route('noticiaTipo', 'eventos') }}">Eventos</a>
            <a class="dropdown-item" href="{{ route('noticiaTipo', 'resultados') }}">Resultados</a>
            <a class="dropdown-item" href="{{ route('noticiaTipo', 'proyectos') }}">Proyectos</a>
          </div>
        </li>
        <li class="nav-item"><a class="nav-link" href="{{ route('contactos') }}">Contactos</a></li>
        <li class="nav-item dropdown">
          <a class="nav-link" href="{{ route('enlaces') }}">
            Enlaces
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="{{ route('enlace', 'estadisticas') }}">Estadísticas</a>
            <a class="dropdown-item" href="{{ route('enlace', 'oportunidades') }}">Oportunidades</a>
            <a class="dropdown-item" href="{{ route('enlace', 'institucionales') }}">Institucionales</a>
          </div>
        </li>
      </ul>
      <a href="https://www.facebook.com/RedCMPVES/" target="_blank" class="nav-item px-3 btn btn-flat btn-f"><i class="fa fa-facebook fa-lg"></i></a>
      {{-- <a href="https://twitter.com/REDCMPVES" target="_blank" class="nav-item px-3 btn btn-flat btn-t"><i class="fa fa-twitter fa-lg"></i></a> --}}
      {{-- <a href="" target="_blank" class="px-3 btn btn-flat btn-y"><i class="fa fa-youtube fa-lg white-text"></i></a> --}}
    </div>
  </div>
</nav>
