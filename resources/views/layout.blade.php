<!DOCTYPE html>
<html class="full-height" lang="es">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Red CMPVES - @yield('title')</title>

  <meta name="description" content="Somos un espacio de coordinación nacional conformada por los CMPVES de un grupo de municipios que vienen trabajando el tema de prevención de la violencia.">
  <meta name="keywords" content="red cmpves el salvador, cmpves el salvador, prevencion el salvador, violencia el salvador.">
  <meta name="Reply-to" content="info@redcmpves.org">
  <meta name="robots" content="index, follow" >
  <meta name="author" content="Jesus Alvarado">
  <link rel="shortcut icon" type="image/ico" href="{{ asset('/favicon.ico') }}"/>

  <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/mdb.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">

  @yield('css')

</head>

<body id="top" data-spy="scroll" data-target=".navbar" data-offset="50">

  <div id="loading">
    <div id="loading-content" class="d-flex justify-content-center align-items-center">
      <img src="{{ asset('/img/loading.gif') }}" alt="Icono" height="80">
    </div>
  </div>
  
  @include('menu')
  
    @yield('content')

  @include('footer')

  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/mdb.min.js') }}"></script>
  <script src="{{ asset('js/main.js') }}"></script>

  @yield('js')

</body>
</html>
