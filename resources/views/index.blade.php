@extends('layout')

@section('title')
	El Salvador
@endsection

@section('content')

	@include('home.header')
	<div id="content">
		@include('home.principios')
		@include('home.somos')
		@include('home.servicios')
		@include('red.mapa')
		@include('home.equipo')
		@include('home.noticias')
		@include('home.contactos')
	</div>

@endsection

@section('js')
	<script src="{{ asset('js/map/imageMapResizer.min.js') }}"></script>
	<script>
		$('map').imageMapResize();
	</script>
@endsection