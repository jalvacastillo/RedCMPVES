@extends('layout')
@section('title')
	Enlaces
@endsection
@section('content')

	@include('enlaces.header')

	<div id="content">
		@include('enlaces.filtro')
		@include('enlaces.listado')
	</div>

@endsection