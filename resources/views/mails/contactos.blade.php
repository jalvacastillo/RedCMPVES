<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="UTF-8">
	<title>Mensaje</title>
</head>

<body style="font-family: arial, helvetica; color: #555; background-color: #eee;" bgcolor="#eee">
	
	<section style="width: 95%; text-align: center; border-radius: 15px; background-color: #fff; margin: auto; padding: 10px;">
		<header>
			<img src="{{ asset('img/logo.jpg') }}" width="150" alt="Logo RedCMPVES">
		</header>
		<div class="margin" style="width: 75%; margin: 30px auto; border: .1px solid #eee;"></div>
	    <article style="width: 95%; text-align: justify; margin: auto;">
	    	
		     @foreach($data as $clave=>$valor)
		       <p> <b>{{ strtoupper($clave) }}</b>: {{$valor}}</p>
		     @endforeach
	    	
	    </article>
		<div class="margin" style="width: 75%; margin: 30px auto; border: .1px solid #eee;"></div>
		<footer>
			<p style="margin: 5px;">RedCMPVES &copy 2018</p>
		</footer>
	</section>

</body>
</html>
