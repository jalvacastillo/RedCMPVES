@extends('layout')
@section('title')
	Gestión del Conocimiento
@endsection
@section('content')

	@include('conocimientos.header')

	<div id="content">
		@include('conocimientos.filtros')
		@include('conocimientos.listado')
	</div>

@endsection