<form action="{{ route('conocimiento-buscar') }}" method="POST" autocomplete="off">
 {{ csrf_field() }}
<div class="container-fluid bg-light wow fadeInDown" data-wow-delay="0.3s">
	<div class="row align-items-center justify-content-center">
        <div class="col-lg-4 col-md-4 col-5 pt-3">
           <div class="form-group" >
              <input type="search" name="nombre" class="form-control bg-white" placeholder="Buscar..." style="height: 40px;padding: 3px 10px;box-sizing: border-box;">
           </div>
        </div>
        <div class="col-lg-3 col-md-3 col-4 pt-3">
            <div class="form-group bg-white">
              <select name="tipo_id" class="form-control" style="height: 40px;">
                <option selected disabled>Todos</option>
                <option value="1">Marco normativo</option>
                <option value="2">Diagnósticos y planes de prevención</option>
                <option value="3">Herramientas para la prevención</option>
                <option value="4">Buenas prácticas</option>
              </select>
            </div>
        </div>
        <div class="col-lg-1 col-md-2 col-3">
           <button type="submit" class="btn btn-primary btn-block btn-sm waves-effect waves-light" style="height: 40px;"><i class="fa fa-search"></i></button>
        </div>
	</div>
</div>
</form>
