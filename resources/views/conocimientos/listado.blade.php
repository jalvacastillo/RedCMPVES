<section class="text-center py-5" id="pricing">
  <div class="container">

    <div class="row mb-lg-4 center-on-small-only wow zoomIn">
      
      @foreach ($documentos as $documento)
      <div class="col-lg-4 col-md-6 col-12 mb-r wow fadeInUp" data-wow-delay=".{{ $loop->index }}s">
        <div class="card">
          <i class="pt-4 fa fa-file-pdf-o fa-2x"></i>
          <div class="card-body">
          <h4 class="card-title">{{ $documento->nombre }}</h4>
          <span class="badge @if ($documento->tipo_id == 1) badge-success @endif @if ($documento->tipo_id == 2) badge-primary @endif @if ($documento->tipo_id == 3) badge-warning @endif @if ($documento->tipo_id == 4) badge-default @endif">
            {{ $documento->tipo()->first()->nombre }}
          </span>
          <p class="card-text grey-text">{{ $documento->descripcion }}</p>
          </div>
          <div class="card-footer">
            <a href="{{ asset('storage/documentos/'.$documento->url) }}" target="_blank" class="card-link">Abrir Documento</a>
          </div>
        </div>
      </div>
      @endforeach

      @if ($documentos->count() <= 0)
        <div class="col-12 text-center">
          <i class="fa fa-empty"></i>
          <p class="text-muted">No hay documentos</p>
          <a href="javascript:window.history.back();" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
        </div>
      @endif

    </div>

    <div class="row">
      <div class="col-12 text-center">
          {{ $documentos->links() }}
      </div>
    </div>

  </div>
</section>