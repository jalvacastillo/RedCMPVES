<section class="py-5" id="eventos">
  <div class="container">

    <div class="row mb-lg-4 center-on-small-only">
      
      @foreach ($noticias as $noticia)
      <div class="col-lg-4 col-md-12 mb-r wow fadeInUp" data-wow-delay=".{{ $loop->index }}s">
          <img class="img-fluid rounded z-depth-1 mb-3" src="{{ asset('storage/noticias/'.$noticia->img) }}" alt="{{ $noticia->titulo }}"/>
          <div class="h4">{{ $noticia->titulo }}</div>
          <h6 class="red-text"> {{ $noticia->fecha->format('d/m/Y') }} | <span class="badge badge-primary">{{ $noticia->categoria()->first()->nombre }}</span></h6>
          <p class="grey-text">{{ $noticia->descripcion }}</p>
          <a href="{{ route('noticia', $noticia->slug) }}" class="wow fadeInRight" data-wow-delay=".5s">Leer más <i class="fa fa-arrow-right"></i></a>
      </div>
      @endforeach

      @if ($noticias->count() <= 0)
        <div class="col-12 text-center">
          <i class="fa fa-empty"></i>
          <p class="text-muted">No hay noticias aún</p>
          <a href="javascript:window.history.back();" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
        </div>
      @endif


    </div>
    <div class="row">
      <div class="col-12 text-center">
          {{ $noticias->links() }}
      </div>
    </div>

  </div>
</section>