@extends('layout')
@section('title')
	Noticias
@endsection
@section('content')

	@include('noticias.header')

	<div id="content">
		@include('noticias.lista')
	</div>

@endsection