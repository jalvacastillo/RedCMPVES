<!DOCTYPE html>
<html class="full-height" lang="es">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Red CMPVES - {{ $noticia->titulo }}</title>

  <meta name="description" content="{{ $noticia->descripcion }}">
  <meta name="keywords" content="eventos, resultados, proyectos">
  <meta name="Reply-to" content="info@redcmpves.org">
  <meta name="robots" content="index, follow" >
  <meta name="author" content="Jesus Alvarado">
  <link rel="shortcut icon" type="image/ico" href="/favicon.ico"/>

  <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/mdb.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">

  <!-- Face comments -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/es_ES/sdk.js#xfbml=1&autoLogAppEvents=1&version=v3.1&appId=1986873098267766';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

  <meta property="og:url"           content="{{ url($noticia->slug) }}" />
  <meta property="og:type"          content="{{ $noticia->categoria()->first()->nombre }}" />
  <meta property="og:title"         content="{{ $noticia->titulo }}" />
  <meta property="og:description"   content="{{ $noticia->descripcion }}" />
  <meta property="og:image"         content="{{ asset('storage/noticias/'.$noticia->img) }}" />

  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@redcmpves">
  <meta name="twitter:title" content="{{ $noticia->titulo }}">
  <meta name="twitter:description" content="{{ $noticia->descripcion }}">
  <meta name="twitter:creator" content="@redcmpves">
  <meta name="twitter:image" content="{{ asset('storage/noticias/'.$noticia->img) }}">
  <meta name="twitter:domain" content="http://redcmpves.org">

</head>

<body id="top" data-spy="scroll" data-target=".navbar" data-offset="50">
  
  @include('menu')

    <header style="height: 200px; background-image: url('{{ asset('img/header.jpg') }}'); background-position: top;">
      <div class="full-bg-img d-flex align-items-center">
        <div class="container">
          <div class="row no-gutters">
          </div>
        </div>
      </div>
    </header>

    <section class="py-5 my-5" id="noticia" style="margin-top: -100px !important;">
      <div class="container">

        <div class="row mb-lg-4 center-on-small-only">
          
          <div class="col-12 mb-r text-center">
              <img class="rounded z-depth-1 mb-3 wow fadeInUp" data-wow-delay=".3s" src="{{ asset('storage/noticias/'.$noticia->img) }}" alt="{{ $noticia->nombre }}" height="300px" />
              <div class="h4 wow fadeInUp" data-wow-delay=".6s">{{ $noticia->nombre }}</div>
              <h5 class="red-text wow fadeInUp" data-wow-delay=".9s"> {{ \Carbon\Carbon::parse($noticia->fecha)->format('d-m-Y') }}  | <span class="badge badge-primary">{{ $noticia->categoria()->first()->nombre }}</span></h5>
              <div class="col-md-12 text-center">
                  <div class="btn-group" role="group">
                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ url('/noticia/' . $noticia->slug) }}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-facebook"></i></a>
                    <a href="http://www.twitter.com/share?url={{ url('/noticia/' . $noticia->slug) }}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank" class="btn btn-info btn-sm"><i class="fa fa-twitter"></i></a>
                  </div>
              </div>
              <br>
              <h1 class="h1-responsive font-bold wow fadeInUp" data-wow-delay="1.2s">{{ $noticia->titulo }}</h1>
          </div>
          <div class="col-12 mb-r wow fadeInUp" data-wow-delay=".3s">
            {!! $noticia->contenido !!}
          </div>
        </div>

        <div class="row">
            <div class="col-12 text-center">
                <div class="fb-comments" data-href="{{ url($noticia->slug) }}" data-width="100%" data-numposts="5"></div>
            </div>
        </div>


      </div>
    </section>

  
  @include('footer')

  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/mdb.min.js') }}"></script>

</body>
</html>
