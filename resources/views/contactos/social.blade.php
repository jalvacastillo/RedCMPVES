<section class="row no-gutters" id="features">
  <div class="container">
    <div class="row wow fadeIn">
        <div class="col-sm-12">
            <h2 class="h1 mt-5 pt-2 pb-5 text-center">Síguenos en nuestra redes sociales</h2>
        </div>
    </div>
    <div class="row mb-2 justify-content-center">
      <div class="col-4 blue text-white">
        <a href="https://www.facebook.com/RedCMPVES/" target="_blank" class="text-white">
        <div class="p-4 text-center wow zoomIn" data-wow-delay=".1s"><i class="fa fa-facebook fa-2x"></i>
          <div class="h5 mt-3">Facebook</div>
          <p class="mt-2">@RedCMPVES</p>
          <p class="mt-2">+100</p>
        </div>
        </a>
      </div>
      <div class="col-4 blue lighten-1 text-white">
        <a href="https://www.facebook.com/RedCMPVES/" target="_blank" class="text-white">
        <div class="p-4 text-center wow zoomIn" data-wow-delay=".3s"><i class="fa fa-twitter fa-2x"></i>
          <div class="h5 mt-3">Twetter</div>
          <p class="mt-2">@RedCMPVES</p>
          <p class="mt-2">+250</p>
        </div>
        </a>
      </div>
      {{-- <div class="col-lg-3 col-md-6 red lighten-1 text-white">
        <a href="https://www.facebook.com/RedCMPVES/" target="_blank" class="text-white">
        <div class="p-4 text-center wow zoomIn" data-wow-delay=".5s"><i class="fa fa-youtube fa-2x"></i>
          <div class="h5 mt-3">Youtube</div>
          <p class="mt-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, nisi.</p>
        </div>
        </a>
      </div>
      <div class="col-lg-3 col-md-6 orange lighten-1 text-white">
        <a href="https://www.facebook.com/RedCMPVES/" target="_blank" class="text-white">
        <div class="p-4 text-center wow zoomIn" data-wow-delay=".7s"><i class="fa fa-instagram fa-2x"></i>
          <div class="h5 mt-3">Instagram</div>
          <p class="mt-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis, consequuntur.</p>
        </div>
      </a>
      </div> --}}
    </div>
    <br><br>
  </div>
</section>