@extends('layout')
@section('title')
	Contactos
@endsection
@section('css')
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script> function recaptcha_callback(){$('#recaptcha').removeAttr('disabled'); } </script>
@endsection

@section('content')

	@include('contactos.header')

	<div id="content">
		{{-- @include('contactos.social') --}}
		@include('red.mapa')
		@include('contactos.form')
	</div>

@endsection

@section('js')
	<script src="{{ asset('js/map/imageMapResizer.min.js') }}"></script>
	<script>
		$('map').imageMapResize();
	</script>
@endsection