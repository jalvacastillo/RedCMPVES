
<section id="contact" style="background-image:url('{{ asset('img/contactos.jpg') }}'); background-attachment: fixed;">
  <div class="rgba-black-strong py-5">
    <div class="container">
      <div class="wow fadeIn">
        <h2 class="h1 text-white pt-5 pb-3 text-center">Contactanos</h2>
        <p class="text-white px-5 mb-3 pb-3 lead text-center">
          Escribenos tus dudas o comentarios.
        </p>
      </div>
      <div class="card mb-5 wow fadeInUp" data-wow-delay=".3s">
        <div class="card-body p-5">
          <div class="row">
            <div class="col-md-7">
              @if(session()->has('message'))
                  <span class="alert alert-info"> {{ session()->get('message') }} </span>
              @endif
              <form action="{{ route('correo') }}" method="POST">
                {{ csrf_field() }}
                
                <div class="row">
                  <div class="col-md-6">
                    <div class="md-form">
                      <input class="form-control" id="nombre" type="text" name="nombre" required="required"/>
                      <label for="nombre">Nombre</label>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="md-form">
                      <input class="form-control" id="correo" type="text" name="correo" required="required"/>
                      <label for="correo">Correo</label>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="md-form">
                      <input class="form-control" id="titulo" type="text" name="titulo" required="required"/>
                      <label for="titulo">Titulo</label>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="md-form">
                      <textarea class="md-textarea" id="mensaje" name="mensaje" required="required"></textarea>
                      <label for="mensaje">Mensaje</label>
                    </div>
                  </div>
                  <div class="input-field col s12">
                   <div class="g-recaptcha" data-sitekey="6LfQ7XEUAAAAAGQ2eXsy9dvenpd4F1bXUovPBcAp" data-callback="recaptcha_callback"></div>
                  </div>
                </div>
                <div class="center-on-small-only mb-4">
                  <button id="recaptcha" class="btn btn-indigo ml-0" type="submit" disabled><i class="fa fa-paper-plane-o mr-2"></i> Enviar</button>                  
                </div>
              </form>
            </div>
            <div class="col-md-5">
              <ul class="list-unstyled text-center">
                <li class="mt-4"><i class="fa fa-map-marker indigo-text fa-2x"></i>
                  <p class="mt-2">El Salvador, C.A.</p>
                </li>
                <li class="mt-4"><i class="fa fa-phone indigo-text fa-2x"></i>
                  <p class="mt-2">(+503) 6198-0408</p>
                </li>
                <li class="mt-4"><i class="fa fa-envelope indigo-text fa-2x"></i>
                  <p class="mt-2">info@redcmpves.org </p>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>