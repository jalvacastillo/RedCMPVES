<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Red CMPVES</title>

  <meta name="description" content="Somos un espacio de coordinación nacional conformada por los CMPVES de un grupo de municipios que vienen trabajando el tema de prevención de la violencia.">
  <meta name="keywords" content="red cmpves el salvador, cmpves el salvador, prevencion el salvador, violencia el salvador.">
  <meta name="Reply-to" content="info@redcmpves.org">
  <meta name="robots" content="index, follow" >
  <meta name="author" content="Jesus Alvarado">
  <link rel="shortcut icon" type="image/ico" href="{{ asset('/favicon.ico') }}"/>

  <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
  
  <style>
    body {
        background: #dedede;
    }
    .page-wrap {
        min-height: 100vh;
    }
  </style>
</head>

<body>

  
  <div class="page-wrap d-flex flex-row align-items-center">
      <div class="container">
          <div class="row justify-content-center">
              <div class="col-md-12 text-center">
                  <span class="display-1 d-block">404</span>
                  <div class="mb-4 lead">La página no ha sido encontrada.</div>
                  <a href="{{ url('/') }}" class="btn btn-link">Regresar</a>
              </div>
          </div>
      </div>
  </div>


</body>
</html>
