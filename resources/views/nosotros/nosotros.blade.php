<section class="text-center py-5 grey lighten-4" id="somos">
  <div class="container">
    <div class="row mb-5 wow fadeIn" data-wow-delay=".3s">
      <div class="col-sm-12">
          <h2 class="h1 pt-5 pb-3">¿Quiénes somos?</h2>
      </div>
      <div class="col-sm-5 wow fadeInLeft" data-wow-delay=".3s">
        <img src="img/logo.jpg" alt="Logo Red CMPVES" width="100%">
      </div>
      <div class="col-sm-7 wow fadeInRight" data-wow-delay=".3s">
        <p class="text-justify lead blue-grey-text"><br>
            La "Red de Comités Municipales de Prevención de la Violencia de El Salvador", ha sido conformada por un grupo de municipios que vienen trabajando desde hace varios años el tema de prevención; así mismo fueron priorizados por la Fundación de Apoyo a Municipios de El Salvador (FUNDAMUNI) en el marco de las políticas públicas de prevención de la violencia que se vienen implementando, entre ellos, el Plan El Salvador Seguro.
        </p>
      </div>
    </div>

    <div class="row my-5 justify-content-md-center" id="naturaleza">
      <div class="col"><a class="btn btn-primary" href="{{ asset('docs/estructura-organizativa.pdf') }}" target="_blank"><i class="fa fa-link"></i> Estructura</a></div>
      <div class="col"><a class="btn btn-primary" href="{{ asset('docs/hoja-de-ruta.pdf') }}" target="_blank"><i class="fa fa-link"></i> Hoja de Ruta</a></div>
      {{-- <div class="col"><a href="{{ route('servicios') }}" target="_blank">Más información <i class="fa fa-link"></i></a></div> --}}
    </div>
    <hr>
    <div class="row pt-5">
      <div class="col-md-6 mb-r wow fadeInUp" data-wow-delay=".3s"><i class="fa fa-building fa-4x orange-text"></i>
        <h3 class="h4 mt-4">Naturaleza</h3>
        <p class="mt-3 blue-grey-text">
          La organización que se crea es una instancia de coordinación nacional, apolítica, no lucrativa, ni religiosa, la cual se denominará "Red de Comités Municipales de Prevención de la Violencia de El Salvador", conformada inicialmente, por un grupo de municipios priorizados en el marco de las políticas públicas de prevención de la violencia, la cual en adelante se identificará como REDCMPVES.
        </p>
      </div>

      <div class="col-md-6 mb-r wow fadeInUp" data-wow-delay=".5s"><i class="fa fa-th-large fa-4x red-text"></i>
        <h3 class="h4 mt-4">Objetivo</h3>
        <p class="mt-4 blue-grey-text">
          Contribuir en la articulación y unión de esfuerzos entre los actores municipales, microregionales,departamentales y nacionales, a través del intercambio de experiencias y la formulación de propuestas que favorezca la construcción de paz y convivencia ciudadana.
        </p>
      </div>
    </div>
    </div>
  </div>
</section>