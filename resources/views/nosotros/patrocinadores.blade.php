<section class="pb-5" id="support">
  <div class="container">
    <div class="wow fadeInUp">
      <h4 class="pt-5 pb-3 text-center text-muted">Gracias al apoyo de</h4>
      {{-- <p class="px-5 mb-5 pb-3 lead blue-grey-text text-center">
        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
      </p> --}}
    </div>

    <div class="row justify-content-md-center">
        
        <div class="col-md-3 wow fadeInUp" data-wow-delay=".3s">
          <div class="card">
            <img class="img-thumbnail" src="{{ asset('img/apoyo/usaid.png') }}" alt="Logo usaid">
          </div>
        </div>
        <div class="col-md-3 wow fadeInUp" data-wow-delay=".6s">
          <div class="card">
            <img class="img-thumbnail" src="{{ asset('img/apoyo/ministerio.png') }}" alt="Logo ministerio de justicia el salvador">
          </div>
        </div>
        <div class="col-md-3 wow fadeInUp" data-wow-delay=".9s">
          <div class="card">
            <img class="img-thumbnail" src="{{ asset('img/apoyo/alemania.png') }}" alt="cooperacion alemana">
          </div>
        </div>
        
    </div>

  </div>
</section>