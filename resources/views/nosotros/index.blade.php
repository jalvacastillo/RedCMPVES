@extends('layout')
@section('title')
	Nosotros
@endsection
@section('content')

	@include('nosotros.header')

	<div id="content">
		@include('nosotros.nosotros')
		@include('nosotros.principios')
		@include('nosotros.equipo')
		@include('nosotros.patrocinadores')
	</div>

@endsection