<header class="header-page" style="background-image: url('{{ asset('/img/header.jpg') }}');">
  <div class="full-bg-img d-flex align-items-center">
    <div class="container">
      <div class="row no-gutters">
        <div class="col-md-10 col-lg-6 text-center text-md-left margins">
            <div class="wow fadeInLeft" data-wow-delay="0.3s">
              <h1 class="h1-responsive font-bold mt-sm-5">Conócenos más</h1>
              <h5>
                Conoce quienes somos, nuestros principios, objetivos, naturaleza y a nuestro equipo.
              </h5>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>