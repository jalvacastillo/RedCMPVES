<section class="py-5" id="equipo">
  <div class="container">
    <div class="wow fadeIn">
      <h2 class="h1 pt-5 pb-3 text-center">Nuestro Equipo</h2>
      <p class="px-5 mb-5 pb-3 lead text-center blue-grey-text">
        Conoce nuestro fabuloso equipo de trabajo.
      </p>
    </div>
    <div class="row mb-lg-4 center-on-small-only">
      
      @foreach ($equipos as $equipo)
      <div class="col-lg-4 col-md-6 col-md-12 mb-r wow fadeInUp" data-wow-delay=".{{ $loop->index }}s">
        <div class="col-md-5 float-left">
          <img class="img-fluid rounded z-depth-1 mb-3" src="{{ asset('img/equipo/'. $equipo->img) }}" alt="{{ $equipo->nombre }}"/>
        </div>
        <div class="col-md-7 float-right">
          <div class="h4">{{ $equipo->nombre }}</div>
          <h6 class="font-bold blue-grey-text mb-2">{{ $equipo->cargo }}</h6>
          <a href="https://twitter.com/{{ $equipo->twitter }}" target="_blank"><i class="fa fa-twitter"></i><span class="ml-1">{{ '@'.$equipo->twitter }}</span></a>
        </div>
      </div>
      @endforeach
      
    </div>
  </div>
</section>