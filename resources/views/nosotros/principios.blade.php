<section class="py-5" id="principios">
  <div class="container">
    <div class="wow fadeIn" data-wow-delay=".1s">
      <h2 class="h1 pt-5 pb-3 text-center">Nuestro Principios</h2>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-6 col-sm-12 green lighten-1 text-white">
        <div class="p-5 text-center wow zoomIn" data-wow-delay=".1s"><i class="fa fa-line-chart fa-2x"></i>
          <div class="h5 mt-2">Pluralidad</div>
          <p class="mt-2">Promover la tolerancia y el respeto a las ideas de todos y todas, buscando siempre el interés común.</p>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 blue lighten-1 text-white">
        <div class="p-5 text-center wow zoomIn" data-wow-delay=".3s"><i class="fa fa-industry fa-2x"></i>
          <div class="h5 mt-2">Transparencia</div>
          <p class="mt-2">Reconocer el derecho de acceso a la información y establecer la rendición de cuentas como práctica periódica.</p>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 purple lighten-1 text-white">
        <div class="p-5 text-center wow zoomIn" data-wow-delay=".5s"><i class="fa fa-users fa-2x"></i>
          <div class="h5 mt-2">Equidad</div>
          <p class="mt-2">Distribuir los recursos técnicos y financieros de acuerdo a las necesidades de los municipios.</p>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 orange lighten-1 text-white">
        <div class="p-5 text-center wow zoomIn" data-wow-delay=".7s"><i class="fa fa-bullhorn fa-2x"></i>
          <div class="h5 mt-2">Inclusión</div>
          <p class="mt-2">Promover la participación efectiva y proactiva sin discriminación, con el fin de que todos y todas formen parte del espacio de planificación y toma de decisiones.</p>
        </div>
      </div>
      <div class="col-lg-8 col-md-12 teal lighten-1 text-white">
        <div class="p-5 text-center wow zoomIn" data-wow-delay=".7s"><i class="fa fa-bullhorn fa-2x"></i>
          <div class="h5 mt-2">Respeto y Promoción de los Derechos Humanos </div>
          <p class="mt-2">Garantizar el reconocimiento de la persona humana como sujeta de derechos.</p>
        </div>
      </div>
    </div>
  </div>
</section>