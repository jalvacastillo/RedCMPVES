<section class="py-5" id="red">
  <div class="container">
    <div class="wow fadeIn">
      <h2 class="h1 pt-5 pb-3 text-center">Nuestros Centros</h2>
      <p class="px-5 mb-5 pb-3 lead text-center blue-grey-text">
        Estos son los 23 centros a nivel nacional.
      </p>
    </div>
    <div class="row mb-lg-4">

      @foreach ($miembros as $miembro)
      <div class="col-lg-6 col-md-12 mb-r wow fadeInUp" data-wow-delay=".{{ $loop->index }}s">
        <div class="card">
            <div class="row">
                <div class="col-3">
                    <img class="card-img-top m-3" src="{{ asset('storage/red/'. $miembro->img) }}" alt="Logo CMPVE de {{ $miembro->nombre }}" />
                </div>
              <div class="card-body col-9">
                <h4 class="card-title">{{ $miembro->nombre }}</h4>
                <span class="label text-muted">{{ $miembro->fecha }}</span>
                <p class="card-text grey-text">Miembro de la red</p>
                @if ($miembro->telefono)
                    <p class="m-0"><a class="text-info" href="tel:+503{{ $miembro->telefono }}"> <i class="fa fa-phone"></i> +503 {{ $miembro->telefono }} </a></p>
                @endif
                @if ($miembro->correo)
                    <p class="m-0"><a class="text-info" href="mailto:{{ $miembro->correo }}"> <i class="fa fa-envelope"></i> {{ $miembro->correo }} </a></p>
                @endif
                @if ($miembro->url_facebook)
                    <p class="m-0"><a class="text-info" href="https://fb.com/{{ $miembro->url_facebook }}" target="_blank"> <i class="fa fa-facebook-square"></i> Facebook</a></p>
                @endif
              </div>
            </div>
          <div class="col-12 card-footer text-center">
            <a href="{{ route('miembro', $miembro->slug) }}" class="card-link">Ver más <i class="fa fa-arrow-right"></i></a>
          </div>
        </div>
      </div>
      @endforeach
      
    </div>
  </div>
</section>