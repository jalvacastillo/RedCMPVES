@extends('layout')

@section('title')
	Red
@endsection

@section('content')

	@include('red.header')

	<div id="content">
		@include('red.ejes')
		@include('red.mapa')
		@include('red.lista')
		@include('red.resultados')
	</div>

@endsection

@section('js')
	<script src="{{ asset('js/map/imageMapResizer.min.js') }}"></script>
	<script>
		$('map').imageMapResize();
	</script>
@endsection