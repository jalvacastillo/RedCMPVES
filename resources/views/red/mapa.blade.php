<section class="text-center py-5 grey lighten-4" id="cmpves">
  <div class="container">
    <div class="row wow fadeIn">
        <div class="col-sm-12">
          <h2 class="h1 pt-5">Conoce nuestra red de CMPVES</h2>
          <p class="text-center lead blue-grey-text"><br>
            Somos 23 Centros Municipales de Prevención de Violencia a Nivel Nacional.
          </p>
      </div>
    </div>

    <div class="mb-lg-4 center-on-small-only wow pulse" data-wow-delay=".5s">
      <p class="img-container">
        
      <img id="mapa" src="{{ asset('img/mapa.png') }}" alt="Mapa red CMPVES" usemap="#interactmap">

      <map name="interactmap" id="interactmap">
        @foreach ($miembros as $miembro)
          <area shape="rect" coords="{{ $miembro->coords}}" data-title="{{-- <img src='{{ asset('img/red/'. $miembro->img) }}'>  --}}{{ $miembro->nombre}}" data-content="
                <p class='card-text grey-text'>Miembro de la red</p>
                @if ($miembro->telefono)
                    <a class='text-info' href='tel:+503{{ $miembro->telefono }}'> <i class='fa fa-phone'></i> </a> <span class='mx-2'>|</span>
                @endif
                @if ($miembro->correo)
                    <a class='text-info' href='mailto:{{ $miembro->correo }}'> <i class='fa fa-envelope'></i> </a> <span class='mx-2'>|</span>
                @endif
                @if ($miembro->url_facebook)
                    <a class='text-info' href='https://fb.com/{{ $miembro->url_facebook }}' target='_blank'> <i class='fa fa-facebook-square'></i> </a>
                @endif
                <div class='col-12 map-popover-footer text-center'>
                  <a href='{{ route('miembro', $miembro->slug) }}' class='card-link'>Ver más <i class='fa fa-arrow-right'></i></a>
                </div>
                "/>
        @endforeach
      </map>

      <div class="map-popover" role="tooltip">
        <div class="map-arrow"></div>
        <h3 class="map-popover-header"></h3>
        <div class="map-popover-body"></div>
      </div>

      </p>
    </div>    
    </div>
  </div>
</section>