@extends('layout')

@section('content')

    <header style="height: 200px; background-image: url('{{ asset('img/header.jpg') }}'); background-position: top;">
      <div class="full-bg-img d-flex align-items-center">
        <div class="container">
          <div class="row no-gutters">
          </div>
        </div>
      </div>
    </header>

  <div id="content">

    <section class="py-5 my-5" id="noticia" style="margin-top: -100px !important;">
      <div class="container">

        <div class="row center-on-small-only">
          
          <div class="col-12 text-center">
              <img height="220" class="rounded z-depth-1 p-2 wow fadeInUp" data-wow-delay=".3s" src="{{ asset('storage/red/' . $miembro->img) }}" alt="{{ $miembro->nombre }}"/>
          </div>

          </div>
          <div class="card-body col-12 text-center">
            <h4 class="card-title">{{ $miembro->nombre }}</h4>
            <span class="label text-muted">{{ $miembro->fecha }}</span>
            <p class="card-text grey-text">Miembro de la red</p>
            @if ($miembro->telefono)
                <a class="text-info" href="tel:+503{{ $miembro->telefono }}"> <i class="fa fa-phone"></i> {{ $miembro->telefono }} </a> <span class="mx-5">|</span>
            @endif
            @if ($miembro->correo)
                <a class="text-info" href="mailto:{{ $miembro->correo }}"> <i class="fa fa-envelope"></i> {{ $miembro->correo }} </a> <span class="mx-5">|</span>
            @endif
            @if ($miembro->url_facebook)
                <a class="text-info" href="https://fb.com/{{ $miembro->url_facebook }}" target="_blank"> <i class="fa fa-facebook-square"></i> Facebook </a>
            @endif
          </div>

        </div>

      </div>
    </section>
  </div>
  
  <section class="container-fluid text-center bg-light p-lg-2" id="pricing">

      <div class="row p-4 center-on-small-only wow zoomIn">
        <h2 class="col-12">Documentos <small class="badge badge-info">{{ $miembro->documentos()->count() }}</small></h2>
        
        @foreach ($miembro->documentos as $doc)
        <div class="col-lg-4 col-md-12 mb-r wow fadeInUp" data-wow-delay=".{{ $loop->index }}s">
          <div class="card">
            <i class="pt-4 fa fa-file-pdf-o fa-2x"></i>
            <div class="card-body">
            <h4 class="card-title">{{ $doc->nombre }}</h4>
            <span class="badge @if ($doc->tipo_id == 1) badge-success @endif @if ($doc->tipo_id == 2) badge-primary @endif @if ($doc->tipo_id == 3) badge-warning @endif @if ($doc->tipo_id == 4) badge-danger @endif">
              {{ $doc->tipo()->first()->nombre }}
            </span>
            <p class="card-text grey-text">{{ $doc->descripcion }}</p>
            </div>
            <div class="card-footer">
              <a href="{{ asset('storage/documentos/miembros/'. $doc->url) }}" target="_blank" class="card-link">Abrir Documento</a>
            </div>
          </div>
        </div>
        @endforeach


        @if ($miembro->documentos->count() <= 0)
          <div class="col-12 text-center">
            <i class="fa fa-empty"></i>
            <p class="text-muted">No ha compartido documentos</p>
          </div>
        @endif

      </div>

  </section>

@endsection