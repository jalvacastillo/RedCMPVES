<section class="text-center py-5 indigo darken-1" id="resultados">
  <div class="container">
    <div class="wow fadeIn">
      <h2 class="h1 pt-5 pb-3 text-white">Resultados</h2>
      <p class="px-5 mb-5 pb-3 lead text-white">
        Estos son algunos resultados del trabajo realizado.
      </p>
    </div>

    <div class="row mb-lg-4 center-on-small-only wow zoomIn">
      
      @foreach ($resultados as $resultado)
      <div class="col-lg-4 col-md-12 mb-r wow fadeInUp" data-wow-delay=".{{ $loop->index }}s">
        <div class="card">
          <img class="card-img-top img-fluid rounded z-depth-1 mb-3" src="{{ asset('storage/noticias/'. $resultado->img) }}" alt="{{ $resultado->titulo }}"/>
          <div class="card-body">
          <h4 class="card-title">{{ $resultado->titulo }}</h4>
          <span class="red-text"> {{ $resultado->fecha->format('d/m/Y') }}</span>
          <p class="card-text grey-text">{{ $resultado->descripcion }}</p>
          </div>
          <div class="card-footer">
            <a href="{{ route('noticia', $resultado->slug) }}" class="card-link">Leer más <i class="fa fa-arrow-right"></i></a>
          </div>
        </div>
      </div>
      @endforeach

    </div>

  </div>
</section>