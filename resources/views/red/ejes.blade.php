<section class="py-5" id="ejes-tematicos">
  <div class="container">
    <div class="wow fadeIn">
      <h2 class="text-center h1 my-4">EJES TEMÁTICOS</h2>
      <p class="px-5 mb-5 pb-3 lead blue-grey-text text-center">
        Trabajamos bajo tres ejes:
      </p>
    </div>

    <div class="row wow fadeInLeft" data-wow-delay=".3s">
        <div class="col-md-6 col-lg-5 col-12 pr-lg-5 pb-5">
            <img class="img-fluid rounded z-depth-2" src="{{ asset('img/servicios/instituciones.jpg') }}" alt="project image"/>
        </div>
        <div class="col-md-6 col-lg-7 col-12 pl-lg-5 pb-4">
        <div class="row mb-3">
            <div class="col-sm-12">
                <h3>Fortalecimiento Institucional</h3>
            </div>
            <ul class="text-justify">
                <li>Proponer estrategias para la articulación y coordinación de actores nacionales, públicos y privados frente a los desafíos y oportunidades que el contexto actual plantea a los CMPV. </li>
                <li>Gestión de recursos técnicos y financieros. </li>
                <li>Fortalecimiento de habilidades técnico-metodológicas en el área de prevención de la violencia.</li>
                <li>Intercambio de experiencias exitosas entre los municipios integrantes de la Red y otros espacios clave, nacionales e internacionales. </li>
                <li>Gestionar el conocimiento facilitando la transmisión de información y habilidades de los miembros de la red. </li>
            </ul>
        </div>
      </div>
    </div>

    <hr/>

    <div class="row d-flex pt-5 wow fadeInRight" data-wow-delay=".3s">
      <div class="order-md-2 order-1 col-md-6 col-lg-5 col-12 pr-lg-5 pb-5"><img class="img-fluid rounded z-depth-2" src="{{ asset('img/servicios/politica.jpg') }}" alt="project image"/></div>
      <div class="order-md-1 order-2 col-md-6 col-lg-7 col-12 pl-lg-5 mb-3">
        <div class="row mb-3">
            <div class="col-sm-12">
                <h3>Incidencia Política</h3>
            </div>
            <ul class="text-justify">
                <li>Visibilizar y posicionar las acciones realizadas por los Comités Municipales de Prevención de la Violencia.</li>
                <li>Formulación de propuestas técnicas para la incidencia en leyes, planes, programas, y proyectos nacionales en prevención de la violencia.</li>
                <li>Pronunciamientos públicos relacionados con la construcción de paz y prevención de la violencia.  </li>
                <li>Creación de pág web u otro tipo de medio con plataforma habilitada para que los CMPV suban información.</li>
            </ul>
        </div>
      </div>
    </div>

        <div class="row pt-5 wow fadeInLeft" data-wow-delay=".3s">

            <div class="col-md-6 col-lg-5 pr-lg-5 pb-5">
                <img class="img-fluid rounded z-depth-2" src="{{ asset('img/servicios/recursos.jpg') }}" alt="project image"/>
            </div>
            <div class="col-md-6 col-lg-7 pl-lg-5 pb-4">
            <div class="row mb-3">
                <div class="col-sm-12">
                    <h3>Gestión de Recursos</h3>
                </div>
                <ul class="text-justify">
                    <li>Promover alianzas público-privado, para la implementación de proyectos y/o acciones concretas de prevención de la violencia locales y regionales.</li>
                    <li>Gestionar cooperación ante instancias públicas y privadas, nacionales e internacionales para el fortalecimiento de la Red y la ejecución de proyectos locales y regionales.</li>
                </ul>
            </div>
          </div>
        </div>

  </div>
</section>