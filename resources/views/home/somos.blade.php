<section class="text-center py-5 grey lighten-4" id="about">
  <div class="container">
    <div class="row wow fadeIn">
        <div class="col-sm-12">
            <h2 class="h1 pt-5 pb-3">¿Quienes somos?</h2>
        </div>
      <div class="col-sm-5 wow fadeInLeft" data-wow-delay="0.3s">
        <img src="img/logo.jpg" alt="Logo Red CMPVES" width="100%">
      </div>
      <div class="col-sm-7 wow fadeInRight" data-wow-delay="0.3s">
        <p class="text-justify lead blue-grey-text"><br>
            La "Red de Comités Municipales de Prevención de la Violencia de El Salvador", ha sido conformada por un grupo de municipios que vienen trabajando desde hace varios años el tema de prevención; así mismo fueron priorizados por la Fundación de Apoyo a Municipios de El Salvador (FUNDAMUNI) en el marco de las políticas públicas de prevención de la violencia que se vienen implementando, entre ellos, el Plan El Salvador Seguro.
        </p>
      </div>
    </div>
    </div>
  </div>
</section>