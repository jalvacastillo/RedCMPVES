<section class="py-5" id="projects">
  <div class="container">
    <div class="wow fadeIn">
      <h2 class="text-center h1 my-4">Servicios</h2>
      <p class="px-5 mb-5 pb-3 lead blue-grey-text text-center">
        Conoce nuestras rutas de incidencia.
      </p>
    </div>

    <div class="row">
        
        @foreach ($servicios as $servicio)
        <div class="col-lg-4 pb-5 wow fadeInUp" data-wow-delay=".{{ $loop->index }}s">
            <img class="img-fluid rounded z-depth-2" src="{{ asset('img/servicios/'. $servicio->img) }}" alt="{{ $servicio->nombre }}"/>
            <h3 class="mt-3">{{ $servicio->nombre }}</h3>
            <p class="text-justify">{{ $servicio->descripcion }}</p>
            <a href="{{ route('red') }}">Más información <i class="fa fa-chevron-circle-right"></i></a>
        </div>
        @endforeach

    </div>

  </div>
</section>