<section class="py-5" id="team">
  <div class="container">
    <div class="wow fadeIn">
      <h2 class="h1 pt-5 pb-3 text-center">Nuestro Equipo</h2>
      <p class="px-5 mb-5 pb-3 lead text-center blue-grey-text">
        Conoce las personas encargadas de revisar y validar las propuestas de programas o proyectos elaborados por los Equipos de Trabajo.
      </p>
    </div>
    <div class="row mb-lg-4 center-on-small-only">
      @foreach ($equipos as $equipo)
      <div class="col-lg-6 col-md-12 mb-r wow fadeInUp" data-wow-delay=".{{ $loop->index }}s">
        <div class="col-md-4 float-left">
          <img class="img-fluid rounded z-depth-1 mb-3" src="{{ asset('img/equipo/'. $equipo->img) }}" alt="{{ $equipo->nombre }}"/>
        </div>
        <div class="col-md-8 float-right">
          <div class="h4">{{ $equipo->nombre }}</div>
          <h6 class="font-bold blue-grey-text mb-4">{{ $equipo->cargo }}</h6>
          <a href="https://twitter.com/{{ $equipo->twitter }}" target="_blank"><i class="fa fa-twitter"></i><span class="ml-1">{{ '@'.$equipo->twitter }}</span></a>
        </div>
      </div>
      @endforeach
    </div>
    <div class="row">
      <div class="col-lg-12  text-center">
        <a href="{{ route('nosotros') }}" style="border-radius: 25px;">Conocer todo el equipo <i class="fa fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>