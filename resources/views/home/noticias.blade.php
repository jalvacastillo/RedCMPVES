<section class="py-5" id="eventos">
  <div class="container">
    <div class="wow fadeIn">
      <h2 class="h1 pt-5 pb-3 text-center">Últimas Noticias</h2>
      <p class="px-5 mb-5 pb-3 lead text-center blue-grey-text">
        Conoce nuestras actividades más recientes.
      </p>
    </div>
    <div class="row center-on-small-only">
      
      @foreach ($noticias as $noticia)
      <div class="col-lg-4 col-md-12 mb-r wow fadeInUp" data-wow-delay=".{{ $loop->index }}s">
          <img class="img-fluid rounded z-depth-1 mb-3" src="{{ asset('storage/noticias/'.$noticia->img) }}" alt="{{ $noticia->titulo }}"/>
          <div class="h4">{{ $noticia->titulo }}</div>
          <h6 class="red-text"> {{ $noticia->fecha->format('d/m/Y') }} | <span class="badge badge-primary">{{ $noticia->categoria()->first()->nombre }}</span></h6>
          <p class="grey-text">{{ $noticia->descripcion }}</p>
          <a href="{{ route('noticia', $noticia->slug) }}" class="wow fadeInRight" data-wow-delay=".5s">Leer más <i class="fa fa-arrow-right"></i></a>
      </div>
      @endforeach

    </div>
    
    <div class="row">
      <div class="col-lg-12  text-center wow pulse" data-wow-delay=".5s">
        <a href="{{ route('noticias') }}" class="btn btn-primary">Ver más noticias</a>
      </div>
    </div>

  </div>
</section>