<header>
<section class="view" id="intro" style="background-image: url('{{ asset('img/header.jpg') }}')">
  <div class="full-bg-img d-flex align-items-center">
    <div class="container">
      <div class="row no-gutters">
        <div class="col-md-12 text-center margins white-gradient">
          <div class="black-text">
            <div>
              <img class="wow fadeInUp" data-wow-delay="1s" src="{{ asset('img/logo.png') }}" alt="Logo RedCMPVES" style="margin: auto;">
              <h5 class="wow fadeInUp" data-wow-delay="1.5s">
                Un espacio  de coordinación nacional donde los CMPV puedan concertar una agenda común sobre los principales retos y desafíos, donde se trabajen propuestas de solución a sus problemas de interés común y se compartan experiencias exitosas que puedan ser adoptadas por los municipios abonando a la operatividad de los CMPVs y del PESS.
              </h5>
            </div>
            <div class="wow fadeInUp" data-wow-delay="1.7s">
              <a class="btn btn-white dark-grey-text font-bold ml-0" data-toggle="modal" href='#presentacion'><i class="fa fa-play mr-1"></i> Ver presentación</a>
              <a class="btn btn-white" href="#about">Conocenos más</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</header>

<div class="modal fade" id="presentacion">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body text-center">
        
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/-d88-mv5oWc?rel=0" allowfullscreen></iframe>
        </div>
        
      </div>
    </div>
  </div>
</div>