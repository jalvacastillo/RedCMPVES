
<section id="contact" style="background-image:url('img/contactos.jpg'); background-attachment: fixed;">
  <div class="rgba-black-strong py-5">
    <div class="container">
      <div class="wow fadeIn">
        <h2 class="h1 text-white pt-5 pb-3 text-center">Contactanos</h2>
        <p class="text-white px-5 mb-3 pb-3 lead text-center">
          Construyamos la Paz y Convivencia Ciudadana. <br>
          Para cualquier duda o consulta puedes ponerte en contacto con nosotros.
        </p>
      </div>

      <div class="row pt-4 mb-5 pb-5">
        <div class="col-lg-12 text-center wow pulse" data-wow-delay=".5s">
          <a href="{{ url('contactos/#contact') }}" class="btn btn-primary">Escribenos</a>
        </div>
      </div>

    </div>
  </div>
</section>