<section class="row no-gutters" id="features">
  <div class="col-lg-2 col-md-4 col-sm-6 col-6 green lighten-1 text-white">
    <div class="p-4 text-center wow zoomIn" data-wow-delay=".1s"><i class="fa fa-line-chart fa-2x"></i>
      <h5 class="h5 mt-3">Pluralidad</h5>
    </div>
  </div>
  <div class="col-lg-2 col-md-4 col-sm-6 col-6 blue lighten-1 text-white">
    <div class="p-4 text-center wow zoomIn" data-wow-delay=".3s"><i class="fa fa-industry fa-2x"></i>
      <h5 class="h5 mt-3">Transparencia</h5>
    </div>
  </div>
  <div class="col-lg-2 col-md-4 col-sm-6 col-6 purple lighten-1 text-white">
    <div class="p-4 text-center wow zoomIn" data-wow-delay=".5s"><i class="fa fa-users fa-2x"></i>
      <h5 class="h5 mt-3">Equidad</h5>
    </div>
  </div>
  <div class="col-lg-2 col-md-4 col-sm-6 col-6 orange lighten-1 text-white">
    <div class="p-4 text-center wow zoomIn" data-wow-delay=".7s"><i class="fa fa-bullhorn fa-2x"></i>
      <h5 class="h5 mt-3">Inclusión</h5>
    </div>
  </div>
  <div class="col-lg-4 col-md-8 col-sm-12 col-12 teal lighten-1 text-white">
    <div class="p-4 text-center wow zoomIn" data-wow-delay=".7s"><i class="fa fa-bullhorn fa-2x"></i>
      <h5 class="h5 mt-3">Respeto y Promoción de los Derechos Humanos </h5>
    </div>
  </div>
</section>