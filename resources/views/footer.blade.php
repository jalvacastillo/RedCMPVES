<footer class="page-footer indigo darken-2 center-on-small-only pt-0 mt-0">
    {{-- <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="mb-5 flex-center">
              <a href="https://www.facebook.com/RedCMPVES/" target="_blank" class="px-3 btn btn-flat btn-f"><i class="fa fa-facebook fa-lg white-text"></i></a>
              <a href="https://twitter.com/REDCMPVES" target="_blank" class="px-3 btn btn-flat btn-t"><i class="fa fa-twitter fa-lg white-text"></i></a>
              <a href="" target="_blank" class="px-3 btn btn-flat btn-y"><i class="fa fa-youtube fa-lg white-text"></i></a>
          </div>
        </div>
      </div>
    </div> --}}
    <div class="footer-copyright">
      <div class="container-fluid">
        <br>
        <p>&copy; 2018 Derechos Reservados <br> Hecho en Ilobasco <i class="fa fa-heart"></i> por <a href="http://websis.me" target="_blank">Websis</a></p> <br>
        {{-- <p>&copy; <a href="/">Material Landing</a> - Design: <a href="https://templateflip.com/" target="_blank">TemplateFlip</a></p> --}}
      </div>
    </div>
  </footer>