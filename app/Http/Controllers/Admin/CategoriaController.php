<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Auth;

use App\Http\Requests\CategoriaRequest;

use App\Models\Blog\Categoria;

class CategoriaController extends Controller
{
    
    public function index(){

        $categorias = Categoria::orderBy('id', 'dsc')->get();

        return view('dash.blog.categorias.index', compact('categorias'));
        
    }

    public function categoria($id){

        $categoria = Categoria::find($id);

        if (!$categoria) {
            $categoria = new Categoria;
        }

        return view('dash.blog.categorias.form', compact('categoria'));
        
    }

    public function guardar(CategoriaRequest $request)
    {
        try {

            if($request->id){
                $categoria = Categoria::find($request->id);
            }
            else{
                $categoria = new Categoria;
            }

            $categoria->fill($request->all());
            $categoria->save();


            flash('Guardado')->success();
            return redirect('dashboard/categoria/'. $categoria->id);

        } catch (Exception $e) {
            flash('No se pudo guardar')->danger();
            return back()->withInput();
        }

    }

    public function delete($id)
    {
        try{

            $noticia = Noticia::find($id);
            $noticia->delete();

            return Response()->json($noticia, 201);

        } catch (Exception $e) {
            return Response()->json($e, 500);
        }

    }


}
