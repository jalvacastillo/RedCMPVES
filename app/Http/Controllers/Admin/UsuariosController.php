<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Mail;
use Auth;

class UsuariosController extends Controller
{


    public function Ver() {
        try {
            // Se cargan todos los clientes de la empresa que no han sido eliminados
            $usuarios = Usuario::where('empresa_id', Auth::user()->empresa->id)
                        ->orderBy('id','dsc')
                        ->get();
            // Se envian los clientes
            return Response::json($usuarios, 200);
            
        } catch (Exception $e) {
            // Si hay error de servidor se envia el error
            return Response::json($e, 500);
        }
    }

    public function auth() {
        try {
            // Se cargan todos los clientes de la empresa que no han sido eliminados
            $usuario = Auth::user();
            // Se envian los clientes
            return Response::json($usuario, 200);
            
        } catch (Exception $e) {
            // Si hay error de servidor se envia el error
            return Response::json($e, 500);
        }
    }

    public function postGuardar()
    {
        try {
            
            // Se reciben los datos y se asigna la empresa.
            $data = Request::all();
            $data['empresa_id'] = Auth::user()->empresa->id;

            // Si verifica si ya existe o si es nuevo
            if(Request::has('id'))
                $usuario = Usuario::find(Request::get('id'));
            else
                $usuario = new Usuario;
            
            // Se guardan los datos y se envia la respuesta
            if($usuario->guardar($data))
                return Response::json($usuario, 201);

            // Si hay errores de validacion se envian
            return Response::json($usuario->errores->all(), 200);

        } catch (Exception $e) {
            // Si hay error de servidor se envia el error
            return Response::json($e, 500);
        }

    }

    public function postEliminar($id)
    {
        try{
            // Se busca al usuario y se elimina
            $usuario = Cliente::find($id);

            $usuario->delete();
            // Se retorna el usuario eliminado
            return Response::json($usuario, 201);

        } catch (Exception $e) {
            // Si hay error de servidor se envia el error
            return Response::json($e, 500);
        }

    }

}
