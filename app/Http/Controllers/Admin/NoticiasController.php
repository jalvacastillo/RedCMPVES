<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\NoticiaRequest;

use App\Models\Noticias\Noticia;
use App\Models\Noticias\Categoria;

class NoticiasController extends Controller
{
    
    public function index(){

        $noticias = Noticia::orderBy('id', 'dsc')->paginate(10);

        return view('admin.noticias.index', compact('noticias'));
        
    }

    public function buscar(Request $request){

        $noticias = Noticia::where('titulo', 'like' ,'%' . $request->nombre . '%')->orderBy('id', 'dsc')->paginate(10);

        return view('admin.noticias.index', compact('noticias'));
        
    }

    public function noticia($id){

        $noticia = Noticia::find($id);

        if (!$noticia) {
            $noticia = new Noticia;
        }

        $categorias = Categoria::all();

        return view('admin.noticias.form', compact('noticia', 'categorias'));
        
    }

    public function noticiaByCategoria($categoria){

        $categoria = Categoria::where('nombre', $categoria)->first();

        $noticias = Noticia::where('categoria_id', $categoria->id)->orderBy('id', 'asc')->get();

        return view('noticias.index', compact('noticias'));
        
    }


    public function guardar(NoticiaRequest $request)
    {
        try {

            // return $request;

            if($request->id){
                $noticia = Noticia::find($request->id);
            }
            else{
                $noticia = new Noticia;
            }

            if ($request->hasFile('file')) {
                if ($noticia->img) {
                    Storage::delete('noticias/'. $noticia->img);
                }
                $nombre = str_slug($request->titulo, '_') . '.' . $request->file->getClientOriginalExtension();
                Storage::putFileAs('noticias', $request->file, $nombre);
                $request['img'] = $nombre;
            }

            if ($request->activo){
                $request['activo'] = true;
            }
            else{
                $request['activo'] = false;
            }

            $noticia->slug  = str_slug($request->titulo, '-');
            $noticia->fill($request->all());
            $noticia->save();

            flash('Guardado')->success();
            return redirect('admin/noticia/'. $noticia->id);

        } catch (Exception $e) {
            flash('No se pudo guardar')->danger();
            return back()->withInput();
        }

    }

    public function delete($id)
    {
        try{

            $noticia = Noticia::find($id);
            if ($noticia->img) {
                Storage::delete('noticias/'. $noticia->img);
            }
            $noticia->delete();

            flash('Eliminado')->success();
            return back()->withInput();

        } catch (Exception $e) {
            flash('No se pudo eliminar')->danger();
            return back()->withInput();
        }

    }


}
