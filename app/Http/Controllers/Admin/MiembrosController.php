<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Http\Requests\MiembroRequest;
use App\Http\Requests\MiembroDocumentoRequest;

use App\Models\Miembros\Miembro;
use App\Models\Miembros\Documento;
use App\Models\Documentos\Tipo;

class MiembrosController extends Controller
{
    
    public function index(){

        $redes = Miembro::orderBy('nombre', 'asc')->get();

        return view('admin.miembro.index', compact('redes'));
        
    }

    public function miembro($id){

        $miembro = Miembro::find($id);

        if (!$miembro) {
            $miembro = new Miembro;
        }

        return view('admin.miembro.form', compact('miembro'));
        
    }


    public function guardar(MiembroRequest $request)
    {
        try {

            if($request->id){
                $red = Miembro::find($request->id);
            }
            else{
                $red = new Miembro;
            }
            
            if ($request->hasFile('file')) {
                if ($red->img){
                    Storage::delete('red/'. $red->img);
                }
                $nombre = str_slug($request->nombre, '_') . '.' . $request->file->getClientOriginalExtension();
                Storage::putFileAs('red', $request->file, $nombre);
                $request['img'] = $nombre;
            } 

            $red->fill($request->all());
            $red->save();

            flash('Guardado')->success();
            return redirect('admin/miembro/'. $red->id);

        } catch (Exception $e) {
            flash('No se pudo guardar')->danger();
            return back()->withInput();
        }

    }

    public function delete($id)
    {
        try{

            $red = Miembro::find($id);
            if ($red->img){
                Storage::delete('red/'. $red->img);
            }
            $red->delete();

            flash('Eliminado')->success();
            return redirect('admin/miembros');

        } catch (Exception $e) {
            flash('No se pudo eliminar')->danger();
            return redirect('admin/miembros');
        }

    }


    public function guardarDocumento(MiembroDocumentoRequest $request)
    {
        try {

            if($request->id){
                $documento = Documento::find($request->id);
            }
            else{
                $documento = new Documento;
            }
            
            if ($request->hasFile('file')) {
                if ($documento->url) {
                    Storage::delete('documentos/miembros/'. $documento->url);
                }
                $nombre = str_slug($request->nombre, '_') . '.' . $request->file->getClientOriginalExtension();
                Storage::putFileAs('documentos/miembros', $request->file, $nombre);
                $request['url'] = $nombre;
            }

            $documento->slug  = str_slug($request->nombre, '-');
            $documento->fill($request->all());
            $documento->save();

            flash('Guardado')->success();
            return redirect('admin/miembro/'. $request->miembro_id);

        } catch (Exception $e) {
            flash('No se pudo guardar')->danger();
            return back()->withInput();
        }

    }

    public function eliminarDocumento($id)
    {
        try {

            $documento = Documento::find($id);

            if ($documento && $documento->url) {
                Storage::delete('documentos/miembros/'. $documento->url);
            }
            $documento->delete();

            flash('Guardado')->success();
             return back()->withInput();

        } catch (Exception $e) {
            flash('No se pudo eliminar')->danger();
            return back()->withInput();
        }

    }

    public function documento($miembro, $id){

        $documento = Documento::find($id);

        if (!$documento) {
            $documento = new Documento;
        }

        $tipos = Tipo::all();

        return view('admin.miembro.form-doc', compact('documento', 'tipos'));
        
    }


}
