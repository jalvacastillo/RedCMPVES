<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Http\Requests\EnlaceRequest;

use App\Models\Enlaces\Enlace;
use App\Models\Enlaces\Tipo;

class EnlacesController extends Controller
{
    
    public function index(){

        $enlaces = Enlace::orderBy('id', 'dsc')->paginate(10);

        return view('admin.enlaces.index', compact('enlaces'));
        
    }

    public function buscar(Request $request){

        $enlaces = Enlace::where('nombre', 'like' ,'%' . $request->nombre . '%')->orderBy('id', 'dsc')->paginate(10);

        return view('admin.enlaces.index', compact('enlaces'));
        
    }

    public function enlace($id){

        $enlace = Enlace::find($id);

        if (!$enlace) {
            $enlace = new Enlace;
        }

        $tipos = Tipo::all();

        return view('admin.enlaces.form', compact('enlace', 'tipos'));
        
    }


    public function guardar(EnlaceRequest $request)
    {
        try {

            if($request->id){
                $enlace = Enlace::find($request->id);
            }
            else{
                $enlace = new Enlace;
            }

            $enlace->fill($request->all());
            $enlace->save();

            flash('Guardado')->success();
            return redirect('admin/enlaces');

        } catch (Exception $e) {
            flash('No se pudo guardar')->danger();
            return back()->withInput();
        }

    }

    public function delete($id)
    {
        try{

            $enlace = Enlace::find($id);
            $enlace->delete();

            flash('Eliminado')->success();
            return back()->withInput();

        } catch (Exception $e) {
            flash('No se pudo eliminar')->danger();
            return back()->withInput();
        }

    }


}
