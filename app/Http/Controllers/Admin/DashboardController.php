<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Noticias\Noticia;
use App\Models\Miembros\Miembro;
use App\Models\Documentos\Documento;
use App\Models\Enlaces\Enlace;

class DashboardController extends Controller
{

    public function index() {

    	$noticias 	= Noticia::count();
    	$redes 		= Miembro::count();
    	$documentos = Documento::count();
    	$enlaces 	= Enlace::count();

        return view('admin.index', compact('noticias', 'redes', 'documentos', 'enlaces'));
    }
    
}
