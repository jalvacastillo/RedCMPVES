<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Http\Requests\DocumentoRequest;

use App\Models\Documentos\Documento;
use App\Models\Documentos\Tipo;

class DocumentosController extends Controller
{
    
    public function index(){

        $documentos = Documento::orderBy('id', 'dsc')->paginate(10);

        return view('admin.documentos.index', compact('documentos'));
        
    }

    public function buscar(Request $request){

        $documentos = Documento::where('nombre', 'like' ,'%' . $request->nombre . '%')->orderBy('id', 'dsc')->paginate(10);

        return view('admin.documentos.index', compact('documentos'));
        
    }

    public function documento($id){

        $documento = Documento::find($id);

        if (!$documento) {
            $documento = new Documento;
        }

        $tipos = Tipo::all();

        return view('admin.documentos.form', compact('documento', 'tipos'));
        
    }


    public function guardar(DocumentoRequest $request)
    {
        try {

            if($request->id){
                $documento = Documento::find($request->id);
            }
            else{
                $documento = new Documento;
            }
            
            if ($request->hasFile('file')) {
                if ($documento->url) {
                    Storage::delete('documentos/'. $documento->url);
                }
                $nombre = str_slug($request->nombre, '_') . '.' . $request->file->getClientOriginalExtension();
                Storage::putFileAs('documentos', $request->file, $nombre);
                $request['url'] = $nombre;
            }

            $documento->slug  = str_slug($request->nombre, '-');
            $documento->fill($request->all());
            $documento->save();

            flash('Guardado')->success();
            return redirect('admin/documentos');

        } catch (Exception $e) {
            flash('No se pudo guardar')->danger();
            return back()->withInput();
        }

    }

    public function delete($id)
    {
        try{

            $documento = Documento::find($id);
            if ($documento->url){
                Storage::delete('documentos/'. $documento->url);
            }
            $documento->delete();

            flash('Eliminado')->success();
            return back()->withInput();

        } catch (Exception $e) {
            flash('No se pudo eliminar')->danger();
            return back()->withInput();
        }

    }


}
