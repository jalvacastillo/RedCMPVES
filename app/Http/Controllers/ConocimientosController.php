<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Documentos\Documento;
use App\Models\Documentos\Tipo;

class ConocimientosController extends Controller
{

    public function index()
    {
        $documentos = Documento::orderBy('id', 'desc')->paginate(12);

        $titulo = 'Gestión del Conocimiento';

        return view('conocimientos.index', compact('documentos', 'titulo'));
    }

    public function tipo($tipo)
    {
        $titulo = 'Gestión del Conocimiento';
        $documentos = Documento::with('tipo')->whereHas('tipo', function($query) use ($tipo){
                                        $query->where('slug', 'like', '%'. $tipo . '%');
                                    })->orderBy('id', 'desc')->paginate(12);

        $tipo = Tipo::where('slug', $tipo)->first();

        if ($tipo) {
            $titulo = $tipo->nombre;
        }


        return view('conocimientos.index', compact('documentos', 'titulo'));
    }

    public function buscar(Request $request)
    {

        $documentos = Documento::with('tipo')
                                    ->when($request->nombre, function($query) use ($request){
                                        return $query->where('nombre', 'like', '%'. $request->nombre . '%');
                                    })
                                    ->when($request->tipo_id, function($query) use ($request){
                                        return $query->where('tipo_id', $request->tipo_id);
                                    })->orderBy('id', 'desc')->paginate(12);
        
        return view('conocimientos.index', compact('documentos'));
    }


}
