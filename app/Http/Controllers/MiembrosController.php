<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Miembros\Miembro;
use App\Models\Servicio;
use App\Models\Noticias\Noticia;

class MiembrosController extends Controller
{

    public function index()
    {
        $miembros = Miembro::orderBy('nombre', 'asc')->get();
        $servicios = Servicio::all();
        $resultados = Noticia::where('categoria_id', 2)->where('activo', true)->take(3)->get();

        return view('red.index', compact('miembros', 'servicios', 'resultados'));
    }

    public function miembro($slug){

    	$miembro = Miembro::where('slug', $slug)->with('documentos')->first();

        return view('red.miembro', compact('miembro'));
        
    }


}
