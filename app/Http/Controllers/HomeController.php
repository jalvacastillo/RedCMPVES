<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Equipo;
use App\Models\Noticias\Noticia;
use App\Models\Servicio;
use App\Models\Miembros\Miembro;

class HomeController extends Controller
{
    public function index()
    {   
        $equipos = Equipo::take(2)->get();
        $noticias = Noticia::take(3)->where('activo', true)->get();
        $servicios = Servicio::all();
        $miembros = Miembro::orderBy('nombre', 'asc')->get();

        return view('index', compact('equipos', 'noticias', 'servicios', 'miembros'));
    }
}
