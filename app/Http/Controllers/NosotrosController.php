<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Equipo;

class NosotrosController extends Controller
{

    public function index()
    {
        $equipos = Equipo::all();

        return view('nosotros.index', compact('equipos'));
    }

    public function noticia($slug){

    	$noticia = Equipo::where('slug', $slug)->first();

        return view('noticias.noticia', compact('noticia'));
        
    }


}
