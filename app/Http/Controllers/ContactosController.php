<?php

namespace App\Http\Controllers;

use Mail;
use Request;
use Carbon\Carbon;
use App\Models\Miembros\Miembro;

class ContactosController extends Controller
{

    public function index()
    {
        $miembros = Miembro::orderBy('nombre', 'asc')->get();

        return view('contactos.index', compact('miembros'));
    }

    public function correo(){


        try {

            $data = Request::except('_token', 'g-recaptcha-response');
                    
            Mail::send('mails.contactos', ['data' => $data], function ($m) use ($data) {
                $m->from('info@redcmpves.org', 'RedCMPVES')
                // ->to('info@redcmpves.org', 'RedCMPVES')
                ->to('coordinadorcmpvecojute@gmail.com', 'RedCMPVES')
                ->cc('alvarado.websis@gmail.com', 'RedCMPVES')
                ->replyTo($data['correo'])
                ->subject($data['titulo']);
            });

            return redirect('contactos/#contact')->with('message', 'Genial, gracias por escribirnos!');

        } catch (Exception $e) {
            
            return redirect('contactos/#contact')->with('message', 'Upps, hubo un error por favor intenta de nuevo!');
            
        }

    }


}
