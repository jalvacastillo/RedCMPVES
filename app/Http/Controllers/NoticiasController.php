<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Noticias\Noticia;
use App\Models\Noticias\Categoria;

class NoticiasController extends Controller
{

    public function index()
    {
        $noticias = Noticia::orderBy('fecha', 'dsc')->where('activo', true)->paginate(12);
        $titulo = 'Noticias';

        return view('noticias.index', compact('noticias', 'titulo'));
    }

    public function tipo($tipo)
    {
        $noticias = Noticia::with('categoria')->whereHas('categoria', function($query) use ($tipo){
                                        $query->where('nombre', 'like', '%'. $tipo . '%');
                                    })->orderBy('id', 'desc')->paginate(12);

        $tipo = Categoria::where('nombre', $tipo)->first();

        if ($tipo) {
            $titulo = $tipo->nombre;
        }

        return view('noticias.index', compact('noticias', 'titulo'));
    }

    public function noticia($slug){

    	$noticia = Noticia::where('slug', $slug)->first();

        return view('noticias.noticia', compact('noticia'));
        
    }


}
