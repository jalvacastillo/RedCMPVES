<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Enlaces\Enlace;
use App\Models\Enlaces\Tipo;

class EnlacesController extends Controller
{

    public function index()
    {
        $enlaces = Enlace::orderBy('id', 'desc')->paginate(12);
        $titulo = 'Enlaces';

        return view('enlaces.index', compact('enlaces', 'titulo'));
    }

    public function tipo($tipo)
    {
        $enlaces = Enlace::with('tipo')->whereHas('tipo', function($query) use ($tipo){
							        	$query->where('slug', 'like', '%'. $tipo . '%');
							        })->orderBy('id', 'desc')->paginate(12);

        $tipo = Tipo::where('slug', $tipo)->first();

        if ($tipo) {
            $titulo = $tipo->nombre;
        }

        return view('enlaces.index', compact('enlaces', 'titulo'));
    }

    public function buscar(Request $request)
    {

        $enlaces = Enlace::with('tipo')
                                    ->when($request->nombre, function($query) use ($request){
                                        return $query->where('nombre', 'like', '%'. $request->nombre . '%');
                                    })
                                    ->when($request->tipo_id, function($query) use ($request){
                                        return $query->where('tipo_id', $request->tipo_id);
                                    })->orderBy('id', 'desc')->paginate(12);
        
        return view('enlaces.index', compact('enlaces'));
    }



}
