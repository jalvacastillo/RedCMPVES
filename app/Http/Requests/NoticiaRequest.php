<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NoticiaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo'        => 'required|unique:noticias,titulo,' . $this->id,
            'file'          => 'image',
            'descripcion'   => 'required',
            'categoria_id'  => 'required',
            'contenido'     => 'required'
        ];
    }

    public function messages()
    {
      return [
        'titulo.unique' => 'Ya hay otra noticia con el mismo nombre'
      ];
    }
}
