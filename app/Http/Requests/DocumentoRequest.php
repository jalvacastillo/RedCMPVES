<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DocumentoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'        => 'required|unique:documentos,nombre,' . $this->id,
            'file'          => 'mimes:pdf|max:40000',
            'tipo_id'       => 'required'

        ];
    }

    public function messages()
    {
      return [
        'nombre.unique' => 'Ya hay otro documento con el mismo nombre'
      ];
    }

}
