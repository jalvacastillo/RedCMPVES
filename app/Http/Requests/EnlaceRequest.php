<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EnlaceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'        => 'required|unique:enlaces,nombre,' . $this->id,
            'url'          =>  'required',
            'tipo_id'       => 'required'

        ];
    }

    public function messages()
    {
      return [
        'nombre.unique' => 'Ya hay otro enlace con el mismo nombre'
      ];
    }
}
