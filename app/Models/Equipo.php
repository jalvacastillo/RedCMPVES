<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Equipo extends Model
{
	use SoftDeletes;
	protected $table = 'equipos';
	protected $fillable = array(
        'nombre',
        'img',
        'cargo',
        'twitter'
	);

    // public function examenes(){
    //     return $this->hasMany('App\Examen', 'escuela_id');
    // }
}
