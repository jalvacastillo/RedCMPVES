<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Empresa extends Model
{
	use SoftDeletes;
	protected $table = 'empresa';
	protected $fillable = array(
        'nombre',
        'logo'
	);

    // public function examenes(){
    //     return $this->hasMany('App\Examen', 'escuela_id');
    // }
}
