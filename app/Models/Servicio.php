<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Servicio extends Model
{
	use SoftDeletes;
	protected $table = 'servicios';
	protected $fillable = array(
        'nombre',
        'img',
        'descripcion',
        'contenido'
	);

    // public function examenes(){
    //     return $this->hasMany('App\Examen', 'escuela_id');
    // }
}
