<?php

namespace App\Models\Noticias;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categoria extends Model
{
	use SoftDeletes;
	protected $table = 'categorias';
	protected $fillable = array(
        'nombre'
	);

    public function noticias(){
        return $this->hasMany('App\Noticia');
    }
}
