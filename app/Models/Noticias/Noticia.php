<?php

namespace App\Models\Noticias;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Noticia extends Model
{
	use SoftDeletes;
	protected $table = 'noticias';
	protected $fillable = array(
        'titulo',
        'slug',
        'img',
        'fecha',
        'categoria_id',
        'descripcion',
        'contenido',
        'activo'
	);

    protected $dates = ['fecha'];
    
    public function getFechaAttribute($value){

        return \Carbon\Carbon::parse($value)->format('Y-m-d');
    }

    public function categoria(){
        return $this->belongsTo('App\Models\Noticias\Categoria', 'categoria_id');
    }
}
