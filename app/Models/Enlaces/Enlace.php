<?php

namespace App\Models\Enlaces;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Enlace extends Model
{
	use SoftDeletes;
	protected $table = 'enlaces';
	protected $fillable = array(
        'nombre',
        'tipo_id',
        'url',
        'descripcion'
	);

    public function tipo(){
        return $this->belongsTo('App\Models\Enlaces\Tipo', 'tipo_id');
    }
}
