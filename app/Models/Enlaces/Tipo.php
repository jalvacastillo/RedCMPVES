<?php

namespace App\Models\Enlaces;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tipo extends Model
{
	use SoftDeletes;
	protected $table = 'tipo_enlaces';
	protected $fillable = array(
        'nombre',
        'slug'
	);

    public function enlaces(){
        return $this->hasMany('App\Models\Enlaces\Enlace');
    }
}
