<?php

namespace App\Models\Miembros;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Miembro extends Model
{
	use SoftDeletes;
	protected $table = 'miembros';
	protected $fillable = array(
        'img',
        'nombre',
        'slug',
        'telefono',
        'correo',
        'departamento',
        'direccion',
        'url_facebook',
        'coords',
        'lat',
        'lon'
	);

    public function documentos(){
        return $this->hasMany('App\Models\Miembros\Documento', 'miembro_id');
    }
}
