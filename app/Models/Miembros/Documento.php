<?php

namespace App\Models\Miembros;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Documento extends Model
{
	use SoftDeletes;
	protected $table = 'miembros_documentos';
	protected $fillable = array(
        'nombre',
        'slug',
        'tipo_id',
        'miembro_id',
        'url',
        'descripcion'
	);

    public function tipo(){
        return $this->belongsTo('App\Models\Documentos\Tipo', 'tipo_id');
    }

    public function miembro(){
        return $this->belongsTo('App\Models\Miembros\Miembro', 'miembro_id');
    }

}
