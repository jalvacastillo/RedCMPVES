<?php

namespace App\Models\Documentos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Documento extends Model
{
	use SoftDeletes;
	protected $table = 'documentos';
	protected $fillable = array(
        'nombre',
        'slug',
        'tipo_id',
        'url',
        'descripcion'
	);

    public function tipo(){
        return $this->belongsTo('App\Models\Documentos\Tipo', 'tipo_id');
    }
}
