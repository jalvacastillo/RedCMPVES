<?php

namespace App\Models\Documentos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tipo extends Model
{
	use SoftDeletes;
	protected $table = 'tipo_documentos';
	protected $fillable = array(
        'nombre',
        'slug'
	);

    public function documentos(){
        return $this->hasMany('App\Models\Documentos\Documento');
    }
}
