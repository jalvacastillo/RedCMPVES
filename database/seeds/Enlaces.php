<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str as Str;

use App\Models\Enlaces\Enlace;
use App\Models\Enlaces\Tipo;

class Enlaces extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

            $tipo = new Tipo;
            $tipo->nombre = 'Estadísticas';
            $tipo->slug = Str::slug($tipo->nombre);
            $tipo->save();
            $tipo = new Tipo;
            $tipo->nombre = 'Oportunidades';
            $tipo->slug = Str::slug($tipo->nombre);
            $tipo->save();
            $tipo = new Tipo;
            $tipo->nombre = 'Institucionales';
            $tipo->slug = Str::slug($tipo->nombre);
            $tipo->save();
            $tipo = new Tipo;
            $tipo->nombre = 'Otro';
            $tipo->slug = Str::slug($tipo->nombre);
            $tipo->save();

            $enlace = new Enlace;
            $enlace->nombre         = 'Caja de Herramientas PREVENIR';
            $enlace->tipo_id        = 4;
            $enlace->url            = 'http://www.gizprevenir.com/cajadeherramientas';
            $enlace->descripcion    = 'Caja de Herramientas para la cooperación intersectorial en prevención de la violencia a nivel local.';
            $enlace->save();  

            $enlace = new Enlace;
            $enlace->nombre         = 'Tu Chance';
            $enlace->tipo_id        = 2;
            $enlace->url            = 'https://tuchance.org/sv';
            $enlace->descripcion    = 'Es una ventana virtual a la oferta de oportunidades de formación, becas, empleos, entrenamiento y capacitación.';
            $enlace->save();         

            // for ($i=0; $i < 30; $i++) { 
            //     $enlace = new Enlace;
            //     $enlace->nombre         = $faker->name;
            //     $enlace->tipo_id        = $faker->numberBetween(1,3);
            //     $enlace->url            = $faker->url;
            //     $enlace->descripcion    = '';
            //     $enlace->save();
            // }

    }
}
