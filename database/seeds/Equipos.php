<?php

use Illuminate\Database\Seeder;
use App\Models\Equipo;

class Equipos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

            $equipo = new Equipo;
            $equipo->nombre        = 'Nelson Villalta';
            $equipo->img           = 'nelson.jpg';
            $equipo->cargo         = 'Coordinador General Nacional';
            $equipo->twitter       = 'Nelson';
            $equipo->save();

            $equipo = new Equipo;
            $equipo->nombre        = 'Roxy Acosta';
            $equipo->img           = 'roxy.jpg';
            $equipo->cargo         = 'Sub Coordinadora Nacional';
            $equipo->twitter       = 'Roxy';
            $equipo->save();

            $equipo = new Equipo;
            $equipo->nombre        = 'Patty';
            $equipo->img           = 'patty.jpg';
            $equipo->cargo         = 'Secretaria';
            $equipo->twitter       = 'Patty';
            $equipo->save();

            $equipo = new Equipo;
            $equipo->nombre        = 'Guillermo Zepeda';
            $equipo->img           = 'guillermo.jpg';
            $equipo->cargo         = 'Tesorero';
            $equipo->twitter       = 'Guillermo';
            $equipo->save();

            $equipo = new Equipo;
            $equipo->nombre        = 'Wigberto Castillo';
            $equipo->img           = 'wigberto.jpg';
            $equipo->cargo         = 'Coordinador Secretaría Comunicación';
            $equipo->twitter       = 'Wigberto';
            $equipo->save();

            $equipo = new Equipo;
            $equipo->nombre        = 'Juan Jose Hernandez';
            $equipo->img           = 'jose.jpg';
            $equipo->cargo         = 'Coordinador de Equipo de Gestión';
            $equipo->twitter       = 'Juan';
            $equipo->save();

            
    }
}
