<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Users::class);
        $this->call(Empresas::class);
        $this->call(Noticias::class);
        $this->call(Miembros::class);
        $this->call(Equipos::class);
        $this->call(Servicios::class);
        $this->call(Documentos::class);
        $this->call(Enlaces::class);
    }
}
