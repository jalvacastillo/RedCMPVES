<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str as Str;

use App\Models\Noticias\Noticia;
use App\Models\Noticias\Categoria;

class Noticias extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

            $categoria = new Categoria;
            $categoria->nombre = 'Noticias';
            $categoria->save();
            $categoria = new Categoria;
            $categoria->nombre = 'Resultados';
            $categoria->save();
            $categoria = new Categoria;
            $categoria->nombre = 'Eventos';
            $categoria->save();
            $categoria = new Categoria;
            $categoria->nombre = 'Proyectos';
            $categoria->save();

            $noticia = new Noticia;
            $noticia->titulo        = 'Asamblea General de miembros CMPV.';
            $noticia->slug          = Str::slug($noticia->titulo);
            $noticia->fecha         = '2018-08-3';
            $noticia->categoria_id  = 1;
            $noticia->img           = '1.jpg';
            $noticia->descripcion   = 'Se sostubo una asamblea con todos lo miembros de los CMPVES.';
            $noticia->contenido     = '<p>Se sostubo una asamblea con todos lo miembros de los CMPVES.</p>';
            $noticia->save();

            $noticia = new Noticia;
            $noticia->titulo        = 'Mesa Principal Foro Taller.';
            $noticia->slug          = Str::slug($noticia->titulo);
            $noticia->fecha         = '2018-08-3';
            $noticia->categoria_id  = 1;
            $noticia->img           = '2.jpg';
            $noticia->descripcion   = 'Asistieron representantes de PREVENIR.GIZ, Subsecrtario de gobernabilidad Viceministerio de Prevensión social.';
            $noticia->contenido     = '<p>Asistieron representantes de PREVENIR.GIZ, Subsecrtario de gobernabilidad Viceministerio de Prevensión social.</p>';
            $noticia->save();

            $noticia = new Noticia;
            $noticia->titulo        = 'Reunión con Instituciones Claves.';
            $noticia->slug          = Str::slug($noticia->titulo);
            $noticia->fecha         = '2018-08-3';
            $noticia->categoria_id  = 1;
            $noticia->img           = '3.jpg';
            $noticia->descripcion   = 'Se sostuvo una reuníón con el subgabinete nacional de prevencion, coordinadores CMPV y tecnicos PREPAZ.';
            $noticia->contenido     = '<p>Se sostuvo una reuníón con el subgabinete nacional de prevencion, coordinadores CMPV y tecnicos PREPAZ.</p>';
            $noticia->save();


            $noticia = new Noticia;
            $noticia->titulo        = 'Diplomado.';
            $noticia->slug          = Str::slug($noticia->titulo);
            $noticia->fecha         = '2018-08-3';
            $noticia->categoria_id  = 2;
            $noticia->img           = '4.jpg';
            $noticia->descripcion   = 'Diplomado UCA, Construccion de paz y seguridad ciudadana.';
            $noticia->contenido     = '<p>Diplomado UCA, Construccion de paz y seguridad ciudadana.</p>';
            $noticia->save();


            $noticia = new Noticia;
            $noticia->titulo        = 'Taller de fortalecimiento.';
            $noticia->slug          = Str::slug($noticia->titulo);
            $noticia->fecha         = '2018-08-3';
            $noticia->categoria_id  = 2;
            $noticia->img           = '5.jpg';
            $noticia->descripcion   = 'Foro taller de fortalecimiento del trabajo de articulacion interinstitucional en la implementacion del PESS, Hotel Sheraton.';
            $noticia->contenido     = '<p>Foro taller de fortalecimiento del trabajo de articulacion interinstitucional en la implementacion del PESS, Hotel Sheraton.</p>';
            $noticia->save();


            $noticia = new Noticia;
            $noticia->titulo        = 'Gestion de Recursos.';
            $noticia->slug          = Str::slug($noticia->titulo);
            $noticia->fecha         = '2018-08-3';
            $noticia->categoria_id  = 2;
            $noticia->img           = '6.jpg';
            $noticia->descripcion   = 'Visita G.I.Z. gestión de asistencia para Foro-Taller.';
            $noticia->contenido     = '<p>Visita G.I.Z. gestión de asistencia para Foro-Taller.</p>';
            $noticia->save();

            
    }
}
