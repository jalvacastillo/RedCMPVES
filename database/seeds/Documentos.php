<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str as Str;

use App\Models\Documentos\Documento;
use App\Models\Documentos\Tipo;

class Documentos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

            $tipo = new Tipo;
            $tipo->nombre = 'Marco normativo';
            $tipo->slug = Str::slug($tipo->nombre);
            $tipo->save();
            $tipo = new Tipo;
            $tipo->nombre = 'Diagnósticos y planes de prevención';
            $tipo->slug = Str::slug($tipo->nombre);
            $tipo->save();
            $tipo = new Tipo;
            $tipo->nombre = 'Herramientas para la prevención';
            $tipo->slug = Str::slug($tipo->nombre);
            $tipo->save();
            $tipo = new Tipo;
            $tipo->nombre = 'Buenas Prácticas';
            $tipo->slug = Str::slug($tipo->nombre);
            $tipo->save();

            Documento::create(['nombre' => 'Apuntes y mensajes para familias miles de manos','slug' => 'apuntes-y-mensajes-para-familias-miles-de-manos','url' => 'apuntes_y_mensajes_para_familias_miles_de_manos.pdf','tipo_id' => '3','descripcion' => NULL]);
            Documento::create(['nombre' => 'Bases y Orientaciones','slug' => 'bases-y-orientaciones','url' => 'bases_y_orientaciones.pdf','tipo_id' => '3','descripcion' => NULL]);
            Documento::create(['nombre' => 'Código Municipal','slug' => 'codigo-municipal','url' => 'codigo_municipal.pdf','tipo_id' => '1','descripcion' => NULL]);
            Documento::create(['nombre' => 'Estategia Nacional de Prevención de Violencia','slug' => 'estategia-nacional-de-prevencion-de-violencia','url' => 'estategia_nacional_de_prevencion_de_violencia.pdf','tipo_id' => '1','descripcion' => NULL]);
            Documento::create(['nombre' => 'Guia de facilitación componente escuela miles de manos','slug' => 'guia-de-facilitacion-componente-escuela-miles-de-manos','url' => 'guia_de_facilitacion_componente_escuela_miles_de_manos.pdf','tipo_id' => '3','descripcion' => NULL]);
            Documento::create(['nombre' => 'Guia de facilitación componente familia','slug' => 'guia-de-facilitacion-componente-familia','url' => 'guia_de_facilitacion_componente_familia.pdf','tipo_id' => '3','descripcion' => NULL]);
            Documento::create(['nombre' => 'Guía de facilitación componente puente miles de manos','slug' => 'guia-de-facilitacion-componente-puente-miles-de-manos','url' => 'guia_de_facilitacion_componente_puente_miles_de_manos.pdf','tipo_id' => '3','descripcion' => NULL]);
            Documento::create(['nombre' => 'Ley Especial Integral para una vida libre de Violencia hacia las Mujeres','slug' => 'ley-especial-integral-para-una-vida-libre-de-violencia-hacia-las-mujeres','url' => 'ley_especial_integral_para_una_vida_libre_de_violencia_hacia_las_mujeres.pdf','tipo_id' => '1','descripcion' => NULL]);
            Documento::create(['nombre' => 'Ley FODES','slug' => 'ley-fodes','url' => 'ley_fodes.pdf','tipo_id' => '1','descripcion' => NULL]);
            Documento::create(['nombre' => 'Ley General de Juventud y su reglamento','slug' => 'ley-general-de-juventud-y-su-reglamento','url' => 'ley_general_de_juventud_y_su_reglamento.pdf','tipo_id' => '1','descripcion' => NULL]);
            Documento::create(['nombre' => 'Ley Lepina','slug' => 'ley-lepina','url' => 'ley_lepina.pdf','tipo_id' => '1','descripcion' => NULL]);
            Documento::create(['nombre' => 'Ley Marco Para la Convivencia Ciudadana y Contraversiones Administrativas','slug' => 'ley-marco-para-la-convivencia-ciudadana-y-contraversiones-administrativas','url' => 'ley_marco_para_la_convivencia_ciudadana_y_contraversiones_administrativas.pdf','tipo_id' => '1','descripcion' => NULL]);
            Documento::create(['nombre' => 'Material de apoyo guias de práctica para docentes','slug' => 'material-de-apoyo-guias-de-practica-para-docentes','url' => 'material_de_apoyo_guias_de_practica_para_docentes.pdf','tipo_id' => '3','descripcion' => NULL]);
            Documento::create(['nombre' => 'Manual Operativo de los Servicios Publicos de Empleo','slug' => 'manual-operativo-de-los-servicios-publicos-de-empleo','url' => 'manual_operativo_de_los_servicios_publicos_de_empleo.pdf','tipo_id' => '4','descripcion' => NULL]);
            Documento::create(['nombre' => 'Pla El Salvador Seguro PESS','slug' => 'pla-el-salvador-seguro-pess','url' => 'pla_el_salvador_seguro_pess.pdf','tipo_id' => '1','descripcion' => NULL]);
            Documento::create(['nombre' => 'Política de Cohesión Social y Prevención AMSS','slug' => 'politica-de-cohesion-social-y-prevencion-amss','url' => 'politica_de_cohesion_social_y_prevencion_amss.pdf','tipo_id' => '1','descripcion' => NULL]);
            Documento::create(['nombre' => 'Política Nacional de Justicia','slug' => 'politica-nacional-de-justicia','url' => 'politica_nacional_de_justicia.pdf','tipo_id' => '1','descripcion' => NULL]);
            Documento::create(['nombre' => 'Política Nacional de Juventud','slug' => 'politica-nacional-de-juventud','url' => 'politica_nacional_de_juventud.pdf','tipo_id' => '1','descripcion' => NULL]);

            // for ($i=0; $i < 30; $i++) { 

            //     $documento = new Documento;
            //     $documento->nombre        = $faker->name;
            //     $documento->slug          = Str::slug($documento->nombre);
            //     $documento->tipo_id       = $faker->numberBetween(1,4);
            //     $documento->url           = $documento->slug . '.pdf';
            //     $documento->descripcion   = '';
            //     $documento->save();
            // }

    }
}
