<?php

use Illuminate\Database\Seeder;
use App\User;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

            $user = new User;
            $user->name = 'Wigberto Castillo';
            $user->email = 'coordinadorcmpvecojute@gmail.com';
            $user->password = Hash::make('@w1gb3rto');
            $user->save();

            $user = new User;
            $user->name = 'Jesus Alvarado';
            $user->email = 'alvarado.websis@gmail.com';
            $user->password = Hash::make('jaac@tic#');
            $user->save();

            $user = new User;
            $user->name = 'Edgardo Ramírez';
            $user->email = 'edgardo.fundamuni@gmail.com';
            $user->password = Hash::make('@3dg4rd0');
            $user->save();

            
    }
}
