<?php

use Illuminate\Database\Seeder;
use App\Models\Servicio;

class Servicios extends Seeder
{

    public function run()
    {
        $faker = Faker\Factory::create();

        $servicio = new Servicio;
        $servicio->nombre        = 'Fortalecimiento Institucional';
        $servicio->descripcion   = 'Se busca la coordinación con actores nacionales, públicos y privados, la gestión de recursos técnicos y financieros, el fortalecimiento de habilidades técnico-metodológicas y el intercambio de experiencias.';
        $servicio->img           = 'instituciones.jpg';
        $servicio->save();

        $servicio = new Servicio;
        $servicio->nombre        = 'Incidencia Política';
        $servicio->descripcion   = 'Buscamos la visibilización y el posicionamiento de las acciones realizadas por los CMPVES, la formulación de leyes, planes, programas, y proyectos nacionales en prevención de la violencia.';
        $servicio->img           = 'politica.jpg';
        $servicio->save();

        $servicio = new Servicio;
        $servicio->nombre        = 'Gestión de Recursos';
        $servicio->descripcion   = 'Buscamos gestionar cooperaciones ante instancias públicas y privadas, nacionales e internacionales para el fortalecimiento de la Red y la ejecución de proyectos locales y regionales.';
        $servicio->img           = 'recursos.jpg';
        $servicio->save();

            
    }
}
