<?php

use Illuminate\Database\Seeder;
use App\Models\Miembros\Miembro;
use App\Models\Miembros\Documento;
use Illuminate\Support\Str as Str;

class Miembros extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

            $red = new Miembro;
            $red->nombre        = 'Mejicanos';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'mejicanos.jpeg';
            $red->departamento  = 'San Salvador';
            $red->telefono      = '';
            $red->correo        = '';
            $red->url_facebook  = 'cmpv.mejicanos';
            $red->coords        = '462,317,484,343';
            $red->save();
            
            $red = new Miembro;
            $red->nombre        = 'Ilobasco';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'ilobasco.jpeg';
            $red->departamento  = 'Cabañas';
            $red->telefono      = '2362-6700';
            $red->correo        = '';
            $red->url_facebook  = 'AlcaldiaIlobasco';
            $red->coords        = '696,230,723,263';
            $red->save();

            $red = new Miembro;
            $red->nombre        = 'Apopa';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'apopa.jpeg';
            $red->departamento  = 'San Salvador';
            $red->telefono      = '';
            $red->correo        = '';
            $red->url_facebook  = 'CMPVApopa';
            $red->coords        = '435,290,463,315';
            $red->save();

            $red = new Miembro;
            $red->nombre        = 'Chalchuapa';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'chalchuapa.jpeg';
            $red->departamento  = 'Santa Ana';
            $red->telefono      = '2461-0568';
            $red->correo        = '';
            $red->url_facebook  = 'AlcaldiaChalchuapaOficial';
            $red->coords        = '217,181,250,209';
            $red->save();

            $red = new Miembro;
            $red->nombre        = 'Armenia';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'armenia.jpeg';
            $red->departamento  = 'Sonsonate';
            $red->telefono      = '';
            $red->correo        = '';
            $red->url_facebook  = 'Alcaldía-Municipal-de-Armenia-Sonsonate-El-Salvador-470689403005884/';
            $red->coords        = '291,310,319,337';
            $red->save();

            $red = new Miembro;
            $red->nombre        = 'Acajutla';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'acajutla.jpeg';
            $red->departamento  = 'Sonsonate';
            $red->telefono      = '2452-3541';
            $red->correo        = '';
            $red->url_facebook  = 'www.acajutla.gob.sv';
            $red->coords        = '159,409,187,436';
            $red->save();

            $red = new Miembro;
            $red->nombre        = 'Usulutan';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'usulutan.jpg';
            $red->departamento  = 'Usulutan';
            $red->telefono      = '2624-4864';
            $red->correo        = 'cmpvjusulutan@gmail.com';
            $red->url_facebook  = 'AlcaldiaUsulutan';
            $red->coords        = '781,475,811,507';
            $red->save();

            $red = new Miembro;
            $red->nombre        = 'Guazapa';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'guazapa.jpg';
            $red->departamento  = 'San Salvador';
            $red->telefono      = '2324-0001';
            $red->correo        = '';
            $red->url_facebook  = 'alcaldiaguazapa';
            $red->coords        = '465,239,497,265';
            $red->save();


            $red = new Miembro;
            $red->nombre        = 'Ciudad Delgado';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'delgado.jpeg';
            $red->departamento  = 'San Salvador';
            $red->telefono      = '2561-2100';
            $red->correo        = '';
            $red->url_facebook  = 'AlcaldiaCiudadDelgadoSV';
            $red->coords        = '491,295,510,331';
            $red->save();


            $red = new Miembro;
            $red->nombre        = 'Olocuilta';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'olocuilta.jpg';
            $red->telefono      = '2561-2100';
            $red->departamento  = 'La Paz';
            $red->correo        = '';
            $red->url_facebook  = 'AlcaldiaDeOlocuilta';
            $red->coords        = '495,412,523,440';
            $red->save();

            $red = new Miembro;
            $red->nombre        = 'Santo Tomas';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'santo_tomas.jpg';
            $red->departamento  = 'San Salvador';
            $red->telefono      = '2213-3100';
            $red->correo        = 'alcaldiadesantotomas@gmail.com';
            $red->url_facebook  = 'AlcaldiaMunicipalDeSantoTomas';
            $red->coords        = '479,365,511,395';
            $red->save();

            $red = new Miembro;
            $red->nombre        = 'Cojutepeque';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'cojutepeque.jpg';
            $red->departamento  = 'Cojutepeque';
            $red->telefono      = '2379-3900';
            $red->correo        = '';
            $red->url_facebook  = 'AlcaldiaMunicipalCojutepeque';
            $red->coords        = '590,319,621,353';
            $red->save();

            $red = new Miembro;
            $red->nombre        = 'San Vicente';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'san_vicente.jpeg';
            $red->departamento  = 'San Vicente';
            $red->telefono      = '2393-7600';
            $red->correo        = 'veroalex90@hotmail.com';
            $red->url_facebook  = 'CMPVSanVicente';
            $red->coords        = '704,354,733,387';
            $red->save();

            $red = new Miembro;
            $red->nombre        = 'Tecoluca';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'tecoluca.jpg';
            $red->departamento  = 'San Vicente';
            $red->telefono      = '';
            $red->correo        = '';
            $red->url_facebook  = 'CMPV-Tecoluca-804889826294075';
            $red->coords        = '655,455,685,487';
            $red->save();

            $red = new Miembro;
            $red->nombre        = 'Ahuachapan';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'ahuachapan.jpg';
            $red->departamento  = 'Ahuachapan';
            $red->telefono      = '2487-4800';
            $red->correo        = '';
            $red->url_facebook  = 'alcaldiadeahuachapan';
            $red->coords        = '123,251,146,270';
            $red->save();

            $red = new Miembro;
            $red->nombre        = 'Sonsonate';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'sonsonate.jpg';
            $red->departamento  = 'Ahuachapan';
            $red->telefono      = '';
            $red->correo        = 'comunicacionessonsonate@gmail.com';
            $red->url_facebook  = 'Alcaldía-Municipal-de-Sonsonate-191014414284574';
            $red->coords        = '223,387,255,415';
            $red->save();

            $red = new Miembro;
            $red->nombre        = 'Zacatecoluca';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'zacatecoluca.jpg';
            $red->departamento  = 'La Paz';
            $red->telefono      = '2334-7900';
            $red->correo        = 'alcaldiadezacatecoluca@gmail.com';
            $red->url_facebook  = 'alcaldiamunicipaldezacatecoluca';
            $red->coords        = '613,510,643,538';
            $red->save();

            $red = new Miembro;
            $red->nombre        = 'Quezaltepeque';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'quezaltepeque.jpg';
            $red->departamento  = 'La Libertad';
            $red->telefono      = '2343-6700';
            $red->correo        = '';
            $red->url_facebook  = 'AlcaldiaQZ';
            $red->coords        = '395,286,427,314';
            $red->save();

            $red = new Miembro;
            $red->nombre        = 'La Unión';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'la_union.jpg';
            $red->departamento  = 'La Unión';
            $red->telefono      = '2609-7000';
            $red->correo        = '';
            $red->url_facebook  = 'AcaldiaLaUnion';
            $red->coords        = '1111,553,1137,587';
            $red->save();

            $red = new Miembro;
            $red->nombre        = 'Izalco';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'izalco.jpg';
            $red->departamento  = 'Sonsonate';
            $red->telefono      = '2429-8000';
            $red->correo        = '';
            $red->url_facebook  = 'IzalcoAlcaldia';
            $red->coords        = '210,312,237,343';
            $red->save();

            $red = new Miembro;
            $red->nombre        = 'Conchagua';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'conchagua.jpg';
            $red->departamento  = 'La Unión';
            $red->telefono      = '2429-8000';
            $red->correo        = '';
            $red->url_facebook  = 'IzalcoAlcaldia';
            $red->coords        = '1081,575,1109,610';
            $red->save();

            $red = new Miembro;
            $red->nombre        = 'Santa Cruz Michapa';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'michapa.jpg';
            $red->departamento  = 'Cojutepeque';
            $red->telefono      = '';
            $red->correo        = '';
            $red->url_facebook  = 'AlcaldiaMichapaOficial';
            $red->coords        = '548,319,577,351';
            $red->save();

            $red = new Miembro;
            $red->nombre        = 'San Salvador';
            $red->slug          = Str::slug($red->nombre);
            $red->img           = 'san_salvador.jpg';
            $red->departamento  = 'San Salvador';
            $red->telefono      = '2511-6000';
            $red->correo        = '';
            $red->url_facebook  = 'AlcaldiaDeSanSalvador';
            $red->coords        = '428,333,459,364';
            $red->save();


            Documento::create(['nombre' => 'Diagnóstico y Plan Ahuachapán Seguro','slug' => 'diagnostico-y-plan-ahuachapan-seguro','url' => 'ahuachapan_diagnostico_y_plan_ahuachapan_seguro.pdf','tipo_id' => '2','miembro_id' => '15','descripcion' => NULL]);
            Documento::create(['nombre' => 'Diagnóstico Municipal de la Violencia de Cojutepeque','slug' => 'diagnostico-municipal-de-la-violencia-de-cojutepeque','url' => 'cojutepeque_diagnostico_municipal_de_la_violencia_de_cojutepeque.pdf','tipo_id' => '2','miembro_id' => '12','descripcion' => NULL]);
            Documento::create(['nombre' => 'Plan Cojute Seguro','slug' => 'plan-cojute-seguro','url' => 'cojutepeque_plan_cojute_seguro.pdf','tipo_id' => '2','miembro_id' => '12','descripcion' => NULL]);
            Documento::create(['nombre' => 'Plan Municipal de Prevención de Violencia Cojutepeque','slug' => 'plan-municipal-de-prevencion-de-violencia-cojutepeque','url' => 'cojutepeque_plan_municipal_de_prevencion_de_violencia_cojutepeque.pdf','tipo_id' => '2','miembro_id' => '12','descripcion' => NULL]);
            Documento::create(['nombre' => 'Política Municipal de Prevención de Violencia','slug' => 'politica-municipal-de-prevencion-de-violencia','url' => 'cojutepeque_politica_municipal_de_prevencion_de_violencia.pdf','tipo_id' => '1','miembro_id' => '12','descripcion' => NULL]);
            Documento::create(['nombre' => 'Diagnóstico de Violencia de Conchagua','slug' => 'diagnostico-de-violencia-de-conchagua','url' => 'conchagua_diagnostico_de_violencia_de_conchagua.pdf','tipo_id' => '2','miembro_id' => '21','descripcion' => NULL]);
            Documento::create(['nombre' => 'PMPV Cojutepeque Seguro','slug' => 'pmpv-cojutepeque-seguro','url' => 'cojutepeque_pmpv_cojutepeque_seguro.pdf','tipo_id' => '1','miembro_id' => '12','descripcion' => NULL]);
            Documento::create(['nombre' => 'Diagnóstico de Guazapa','slug' => 'diagnostico-de-guazapa','url' => 'guazapa_diagnostico_de_guazapa.pdf','tipo_id' => '2','miembro_id' => '8','descripcion' => NULL]);
            Documento::create(['nombre' => 'Plan de Trabajo de Mural','slug' => 'plan-de-trabajo-de-mural','url' => 'guazapa_plan_de_trabajo_de_mural.pdf','tipo_id' => '2','miembro_id' => '8','descripcion' => NULL]);
            Documento::create(['nombre' => 'Plan de Trabajo Orquesta Filarmónica','slug' => 'plan-de-trabajo-orquesta-filarmonica','url' => 'guazapa_plan_de_trabajo_orquesta_filarmonica.pdf','tipo_id' => '2','miembro_id' => '8','descripcion' => NULL]);
            Documento::create(['nombre' => 'Plan Guazapa Seguro','slug' => 'plan-guazapa-seguro','url' => 'guazapa_plan_guazapa_seguro.pdf','tipo_id' => '2','miembro_id' => '8','descripcion' => NULL]);
            Documento::create(['nombre' => 'PMPV de Guazapa','slug' => 'pmpv-de-guazapa','url' => 'guazapa_pmpv_de_guazapa.pdf','tipo_id' => '2','miembro_id' => '8','descripcion' => NULL]);
            Documento::create(['nombre' => 'Plan Ilobasco Seguro','slug' => 'plan-ilobasco-seguro','url' => 'ilobasco_plan_ilobasco_seguro.pdf','tipo_id' => '2','miembro_id' => '2','descripcion' => NULL]);
            Documento::create(['nombre' => 'Ordenanza Contravensional para la Convivencia y Seguridad','slug' => 'ordenanza-contravensional-para-la-convivencia-y-seguridad','url' => 'santa-cruz-michapa_ordenanza_contravensional_para_la_convivencia_y_seguridad.pdf','tipo_id' => '2','miembro_id' => '22','descripcion' => NULL]);
            Documento::create(['nombre' => 'Plan El Salvador Seguro Santa Cruz Michapa','slug' => 'plan-el-salvador-seguro-santa-cruz-michapa','url' => 'santa-cruz-michapa_plan_el_salvador_seguro_santa_cruz_michapa.pdf','tipo_id' => '2','miembro_id' => '22','descripcion' => NULL]);
            Documento::create(['nombre' => 'PMPV San Vivente Seguro','slug' => 'pmpv-san-vivente-seguro','url' => 'san-vicente_pmpv_san_vivente_seguro.pdf','tipo_id' => '1','miembro_id' => '13','descripcion' => NULL]);
            Documento::create(['nombre' => 'Política Municipal de Prevención de la Violencia San Vincente','slug' => 'politica-municipal-de-prevencion-de-la-violencia-san-vincente','url' => 'san-vicente_politica_municipal_de_prevencion_de_la_violencia_san_vincente.pdf','tipo_id' => '1','miembro_id' => '13','descripcion' => NULL]);
            
            Documento::create(['nombre' => 'Diagnostico Situacional de las Mujeres','slug' => 'diagnostico_situacional_de_las_mujeres','url' => 'armenia_diagnostico_situacional_de_las_mujeres.pdf','tipo_id' => '2','miembro_id' => '5','descripcion' => NULL]);
            Documento::create(['nombre' => 'Estatutos de MEPERSA','slug' => 'estatutos_de_mepersa','url' => 'estatutos_de_mepersa.pdf','tipo_id' => '1','miembro_id' => '5','descripcion' => NULL]);
            Documento::create(['nombre' => 'Experiencia Exitosa de la Liga Atletica Municipal Armenia','slug' => 'experiencia_exitosa_de_la_liga_atletica_municipal_armenia','url' => 'armenia_experiencia_exitosa_de_la_liga_atletica_municipal_armenia.pdf','tipo_id' => '1','miembro_id' => '5','descripcion' => NULL]);
            Documento::create(['nombre' => 'Ordenanza Reguladora de uso de los Sitios Públicos','slug' => 'ordenanza_reguladora_de_uso_de_los_sitios_publicos','url' => 'armenia_ordenanza_reguladora_de_uso_de_los_sitios_publicos.pdf','tipo_id' => '1','miembro_id' => '5','descripcion' => NULL]);



            // for ($i=0; $i < 30; $i++) { 

            //     $documento = new Documento;
            //     $documento->nombre        = $faker->name;
            //     $documento->slug          = Str::slug($documento->nombre);
            //     $documento->tipo_id       = $faker->numberBetween(1,4);
            //     $documento->miembro_id    = $faker->numberBetween(1,10);
            //     $documento->url           = $documento->slug . '.pdf';
            //     $documento->descripcion   = '';
            //     $documento->save();
            // }

            
    }
}
