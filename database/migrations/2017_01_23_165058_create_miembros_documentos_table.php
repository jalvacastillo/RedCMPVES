<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMiembrosDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('miembros_documentos', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('nombre');
            $table->string('slug')->unique();
            $table->string('url');
            $table->string('tipo_id');
            $table->integer('miembro_id');
            $table->string('descripcion')->nullable();
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('miembros_documentos');
    }
}
