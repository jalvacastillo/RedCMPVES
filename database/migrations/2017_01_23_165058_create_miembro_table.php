<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMiembroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('miembros', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('nombre');
            $table->string('slug')->unique();
            $table->string('img')->nullable();
            $table->string('telefono')->nullable();
            $table->string('correo')->nullable();
            $table->string('departamento')->nullable();
            $table->text('direccion')->nullable();
            $table->string('url_facebook')->nullable();
            $table->string('url_web')->nullable();
            $table->string('coords')->nullable();
            $table->string('lat')->nullable();
            $table->string('lon')->nullable();
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('miembros');
    }
}
