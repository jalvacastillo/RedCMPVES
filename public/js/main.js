
$(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 1000);
});


$(function () {

    new WOW().init();
    
    // Activar sub menus
    $('ul.navbar-nav li.dropdown').hover(function() {
        if ($(document).width() > 974) {
            $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(300);
        }
    }, function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(300);
    });

    // Mapa interactivo
    $('area').hover(function(evt){
        popover(evt.pageY, evt.pageX, this);
    });
    $('area').click(function(evt){
        popover(evt.pageY, evt.pageX, this);
    });

    function popover(pageY, pageX, el){
        var titulo = $(el).data("title");
        var content = $(el).data("content");

        if (!$('.map-popover').hasClass('show') || $('.map-popover-header').html() != $(el).data("title")) {

            $('.map-popover-header').html(titulo);
            $('.map-popover-body').html(content);
            if ($(document).width() > 550) {
                $('.map-popover').css({'position': 'absolute', 'top': pageY - 210, 'left': pageX - 150 });
            }else{
                $('.map-popover').css({'position': 'absolute', 'top': pageY - 150, 'left': pageX - 150 });
            }

            $('.map-popover').addClass('show');
        }
    }


    $(window).on('click', function(el){
        console.log(el.target.tagName);
        if ($('.map-popover').hasClass('show') && el.target.tagName != 'AREA') {
            $('.map-popover').removeClass('show');
        }
    });


})

$(window).on("load",function(){$('#loading').fadeOut(1000); });
